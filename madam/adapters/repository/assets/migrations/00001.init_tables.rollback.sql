drop table if exists watchfolders;
drop table if exists applications;
drop table if exists jobs;
drop table if exists workflow_instances;
drop table if exists timers;
drop table if exists workflows;
