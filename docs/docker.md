# Docker setup

1. [Install Docker](https://docs.docker.com/engine/install/).

1. [Configure userns-remap](https://docs.docker.com/engine/security/userns-remap/) to map container user `root` to a
   host non-root user.

1. Configure the dev station as a [Docker Swarm Manager](https://docs.docker.com/engine/swarm/).

## Example

Install docker on Ubuntu https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository.

Create the `docker` group and add your user to
it: https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user

Create `madam` group with a huge GID:

    sudo groupadd -g 500000 madam

Create `madam` user with a huge UID:

    sudo useradd -u 500000 -g madam -d /dev/null -s /bin/false madam

Edit `/etc/subgid` and add the group with a subgid range if none:

    madam:500000:65536

Edit `/etc/subuid` and add the user with a subuid range if none:

    madam:500000:65536

Edit /etc/docker/daemon.json and add the `nsusermap`:

    {
        "userns-remap": "madam"
    }

Restart docker daemon (beware that all current images/containers and swarm setup will disappear):

    sudo systemctl restart docker.service

Init swarm manager:

    docker swarm init

## Warning

_Files created by containers will have a umask of 022 by default. So, if the file owner will be the one configured in
the `nsusermap` of docker, permissions to write will only be granted to that user._
