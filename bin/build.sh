#!/usr/bin/env bash

VERSION=`poetry version --short`
MADAM_PACKAGE="madam_mam-${VERSION}-py3-none-any.whl"

# build source and wheel packages for PyPI
poetry build

# build docker image
docker image build --tag madam:${VERSION} --build-arg madam_package=$MADAM_PACKAGE --file docker/Dockerfile dist
