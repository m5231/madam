#!/usr/bin/env bash

# Initialize the variable before combine
FILES=""

for FILE in $(git diff --cached --name-only --diff-filter=ACM -z | xargs -0)
do
    # debug
    # echo "FILE=$FILE";

    # do a check only on the python files
    if [[ ! $FILE =~ (\.py)$ ]]; then continue; fi

    FILES+="${FILE} "
done

# debug
#echo "FILES=$FILES";

# no python files, cancel...
if [[ $FILES == "" ]]; then exit 0; fi

if [[ -z "$VIRTUAL_ENV" ]]; then
    echo "No \$VIRTUAL_ENV set"
else
    echo "\$VIRTUAL_ENV is set to $VIRTUAL_ENV"
fi

python --version

# insert licence
echo "Insert licence with insert_license..."
insert_license --license-filepath=license_header.txt $FILES

# sort import
echo "Sort imports with isort..."
isort -sg *_rc.py -rc $FILES

# format files
echo "Format with black..."
black $FILES

# static typing
echo "Check static typing with mypy..."
mypy --ignore-missing-imports --install-types --non-interactive $FILES

# linter
echo "Lint with ruff..."
ruff $FILES

