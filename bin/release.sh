#!/usr/bin/env bash

VERSION=$1

poetry version ${VERSION}
sed -i "s/__version__ = .*/__version__ = \"${VERSION}\"/" madam/__init__.py
sed -i "s/image: \"madam:.*\"/image: \"madam:${VERSION}\"/" docker/local/docker-compose.yml
git add -u
git commit -m "Version ${VERSION}"
git tag ${VERSION}
