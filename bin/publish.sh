#!/usr/bin/env bash

VERSION=`poetry version --short`

echo "publish Python package to PYPI..."
poetry publish

echo "publish docker image to Docker Hub..."

# ask Docker Hub username
echo "docker_id:"
read DOCKER_ID

docker login --username ${DOCKER_ID}

docker tag madam:${VERSION} ${DOCKER_ID}/madam:${VERSION}
docker push ${DOCKER_ID}/madam:${VERSION}
