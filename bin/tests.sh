#!/usr/bin/env bash

export $(grep -v '^#' .env | xargs -d '\n')

poetry run python -m pytest -x $@
