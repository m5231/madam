# MADAM

MADAM is the Multi Agent Digital Asset Manager.

It provides a three-tier architecture platform to handle workflow processing in a distributed environment.

It uses Docker swarm to dispatch processes in a cluster of machines.

It is a free (as freedom) software written in Python.

## Source code

MADAM is licensed under the [GPLv3 licence](https://www.gnu.org/licenses/gpl-3.0.en.html).

The source code is available as a git repository on Gitlab:

https://gitlab.com/m5231/madam

## Requirements

MADAM docker image requires **to be running on a manager node in a Swarm mode docker network**.

It needs a configuration file written in yaml.
Consult the git repository to see configuration examples.

The container needs to know where to find the configuration file.
Give the path of the file with the dedicated environment variable:

`MADAM_CONFIG_PATH`: path of the configuration file.

## Dependencies

MADAM requires a PostgreSQL server to store data.

## Environment variables

`MADAM_CONFIG_PATH`: path of the configuration file.
`MADAM_LOG_LEVEL`: log level (default: "ERROR")

## Docker compose

A template for your `docker-compose` configuration:

```
secrets:
  madam.yaml:
    file: /path/to/madam.yaml
    
services:
  madam:
    image: vitexier/madam:0.x.y
    environment:
      MADAM_LOG_LEVEL: "DEBUG"
      MADAM_CONFIG_PATH: "/run/secrets/madam.yaml"
    ports:
      - "5000:5000"
    volumes:
        - "data_volume:"
        - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - madam_network
    secrets:
      - madam.yaml
    deploy:
      replicas: 1
      placement:
        constraints: [node.role == manager]
```
