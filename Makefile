.PHONY: tests check format mypy ruff insert-license gitlab-ci-linter environment build publish deploy version databases databases_rollback databases_list

SHELL=/bin/bash
MIGRATIONS_PATH=madam/adapters/repository/assets/migrations

include .env
export

# docker rm all stopped containers
docker_rm_all:
	docker service rm $$(docker service ls -q)

databases:
	@poetry run yoyo apply --batch --no-config-file --database ${DATABASE_URL} ${MIGRATIONS_PATH}
	@poetry run yoyo apply --batch --no-config-file --database ${DATABASE_URL_TESTS} ${MIGRATIONS_PATH}

databases_rollback:
	@poetry run yoyo rollback --batch --no-config-file --database ${DATABASE_URL} ${MIGRATIONS_PATH}
	@poetry run yoyo rollback --batch --no-config-file --database ${DATABASE_URL_TESTS} ${MIGRATIONS_PATH}

databases_list:
	@poetry run yoyo list --no-config-file --database "${DATABASE_URL}" ${MIGRATIONS_PATH}
	@poetry run yoyo list --no-config-file --database "${DATABASE_URL_TESTS}" ${MIGRATIONS_PATH}

# run tests
tests:
	./bin/tests.sh

format:
	poetry run black madam tests

# check static typing
mypy:
	poetry run mypy --ignore-missing-imports --install-types --non-interactive madam tests

# check code errors
ruff:
	poetry run ruff madam
	poetry run ruff tests

# check import sorting
isort:
	poetry run isort -sg *_rc.py -rc madam tests

# check licence headers
insert-license:
	poetry run insert_license --license-filepath=license_header.txt $$(find $$(pwd -P)/madam -type f | grep +*.py$$ | grep -v +*_rc.py$ | sed -z "s/\n/ /g")
	poetry run insert_license --license-filepath=license_header.txt $$(find $$(pwd -P)/tests -type f | grep +*.py$$ | sed -z "s/\n/ /g")

# full check
check: insert-license isort format mypy ruff gitlab-ci-linter

# create database server
environment:
	poetry run ansible-playbook -i hosts.yaml ansible/environment.yaml

# build PyPI packages and docker image
build:
	./bin/build.sh

# build and publish to PYPI and docker hub
publish:
	./bin/build.sh
	./bin/publish.sh

# deploy a local docker container
deploy:
	mkdir /tmp/madam_tests -p
	poetry run ansible-playbook -i hosts.yaml ansible/deploy.yaml
