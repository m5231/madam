# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on mediainfo agent
"""
import json
from pathlib import Path
from time import sleep

from madam.domains.entities.application import (
    STATUS_COMPLETE as APPLICATION_STATUS_COMPLETE,
)
from madam.domains.entities.job import STATUS_COMPLETE as JOB_STATUS_COMPLETE
from madam.domains.entities.workflow_instance import STATUS_COMPLETE, STATUS_RUNNING

WORKFLOW_PATH_DEFAULT = Path(__file__).parent.parent.joinpath(
    "assets/mediainfo/mediainfo.bpmn"
)
DESTINATION_PATH = "/tmp/madam_tests/ffmpeg_video_create.mp4"


def test_mediainfo(application):
    """
    Test success on mediainfo agent

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH_DEFAULT, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_COMPLETE
    assert instances[0].end_at is not None
    assert instances[0].input is None
    assert instances[0].output is not None
    assert instances[0].error is None
    assert "destination" in instances[0].output
    assert "mediainfo" in instances[0].output
    assert "media" in instances[0].output["mediainfo"]
    assert "@ref" in instances[0].output["mediainfo"]["media"]
    assert instances[0].output["mediainfo"]["media"]["@ref"] == DESTINATION_PATH
    assert "track" in instances[0].output["mediainfo"]["media"]
    assert "track" in instances[0].output["mediainfo"]["media"]
    assert len(instances[0].output["mediainfo"]["media"]["track"]) == 2
    track_general = instances[0].output["mediainfo"]["media"]["track"][0]
    assert track_general["@type"] == "General"
    assert track_general["CompleteName"] == DESTINATION_PATH
    # assert track_general["Count"] == "331"
    assert track_general["DataSize"] == "3177"
    assert track_general["Duration"] == "5.000"
    assert track_general["FileSize"] == "5519"
    track_video = instances[0].output["mediainfo"]["media"]["track"][1]
    assert track_video["@type"] == "Video"
    assert track_video["BitDepth"] == "8"
    assert track_video["BitRate"] == "5070"
    assert track_video["CodecID"] == "avc1"
    assert track_video["ColorSpace"] == "YUV"
    # assert track_video["Count"] == "380"
    assert track_video["DisplayAspectRatio"] == "1.222"
    assert track_video["Duration"] == "5.000"

    jobs = application.jobs.list(status=JOB_STATUS_COMPLETE)
    assert len(jobs) == 2
    assert jobs[0].agent_id == "ffmpeg_create_video"
    assert jobs[0].agent_type == "ffmpeg"
    assert jobs[0].input["destination"] == DESTINATION_PATH
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_COMPLETE

    assert jobs[1].agent_id == "mediainfo_agent"
    assert jobs[1].agent_type == "mediainfo"
    assert jobs[1].end_at is not None
    assert jobs[1].status == JOB_STATUS_COMPLETE
    assert jobs[1].input["destination"] == DESTINATION_PATH
    assert jobs[1].output is not None
    assert "mediainfo" in jobs[1].output
    assert "media" in instances[0].output["mediainfo"]
    assert "@ref" in instances[0].output["mediainfo"]["media"]
    assert instances[0].output["mediainfo"]["media"]["@ref"] == DESTINATION_PATH
    assert "track" in instances[0].output["mediainfo"]["media"]
    assert "track" in instances[0].output["mediainfo"]["media"]
    assert len(instances[0].output["mediainfo"]["media"]["track"]) == 2
    track_general = instances[0].output["mediainfo"]["media"]["track"][0]
    assert track_general["@type"] == "General"
    assert track_general["CompleteName"] == DESTINATION_PATH
    # assert track_general["Count"] == "331"
    assert track_general["DataSize"] == "3177"
    assert track_general["Duration"] == "5.000"
    assert track_general["FileSize"] == "5519"
    track_video = instances[0].output["mediainfo"]["media"]["track"][1]
    assert track_video["@type"] == "Video"
    assert track_video["BitDepth"] == "8"
    assert track_video["BitRate"] == "5070"
    assert track_video["CodecID"] == "avc1"
    assert track_video["ColorSpace"] == "YUV"
    # assert track_video["Count"] == "380"
    assert track_video["DisplayAspectRatio"] == "1.222"
    assert track_video["Duration"] == "5.000"

    applications = application.applications.list(status=APPLICATION_STATUS_COMPLETE)
    assert len(applications) == 2
    assert applications[0].job.id == jobs[0].id
    assert applications[0].name == "ffmpeg"
    assert applications[0].end_at is not None
    assert applications[0].status == APPLICATION_STATUS_COMPLETE

    assert applications[1].job.id == jobs[1].id
    assert applications[1].name == "mediainfo"
    assert applications[1].end_at is not None
    assert applications[1].status == APPLICATION_STATUS_COMPLETE
    assert applications[1].logs is not None
    assert json.loads(applications[1].logs) is not None
    logs = json.loads(applications[1].logs)
    assert "media" in logs
    assert "@ref" in logs["media"]
    assert logs["media"]["@ref"] == DESTINATION_PATH
    assert "track" in logs["media"]
    assert "track" in logs["media"]
    assert len(logs["media"]["track"]) == 2
    track_general = logs["media"]["track"][0]
    assert track_general["@type"] == "General"
    assert track_general["CompleteName"] == DESTINATION_PATH
    # assert track_general["Count"] == "331"
    assert track_general["DataSize"] == "3177"
    assert track_general["Duration"] == "5.000"
    assert track_general["FileSize"] == "5519"
    track_video = logs["media"]["track"][1]
    assert track_video["@type"] == "Video"
    assert track_video["BitDepth"] == "8"
    assert track_video["BitRate"] == "5070"
    assert track_video["CodecID"] == "avc1"
    assert track_video["ColorSpace"] == "YUV"
    # assert track_video["Count"] == "380"
    assert track_video["DisplayAspectRatio"] == "1.222"
    assert track_video["Duration"] == "5.000"

    application.workflows.delete(workflow)
