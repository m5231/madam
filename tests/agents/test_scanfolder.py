# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on scanfolder agent
"""
import os
from pathlib import Path
from time import sleep

from madam.domains.entities.job import STATUS_COMPLETE as JOB_STATUS_COMPLETE
from madam.domains.entities.workflow_instance import STATUS_COMPLETE, STATUS_RUNNING

WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/scanfolder/scanfolder.bpmn"
)
WORKFLOW_RECURSIVE_PATH = Path(__file__).parent.parent.joinpath(
    "assets/scanfolder/scanfolder_recursive.bpmn"
)
WORKFLOW_GLOB_PATH = Path(__file__).parent.parent.joinpath(
    "assets/scanfolder/scanfolder_glob.bpmn"
)
SCANFOLDER_PATH = "/tmp/madam_tests/scanfolder"
SUBFOLDER_PATH = str(Path(SCANFOLDER_PATH).joinpath("subfolder"))


def test_scanfolder(application):
    """
    Test success on scanfolder agent

    :param application: Madam Application instance
    :return:
    """
    # create folders
    os.mkdir(SCANFOLDER_PATH)
    os.mkdir(SUBFOLDER_PATH)

    # create fake files
    filepath_01 = Path(SCANFOLDER_PATH).joinpath("video01.mp4")
    filepath_01.touch()
    filepath_02 = Path(SCANFOLDER_PATH).joinpath("video02.mp4")
    filepath_02.touch()
    filepath_03 = Path(SCANFOLDER_PATH).joinpath("video03.mp4")
    filepath_03.touch()
    filepath_04 = Path(SUBFOLDER_PATH).joinpath("video04.mp4")
    filepath_04.touch()
    filepath_05 = Path(SUBFOLDER_PATH).joinpath("video05.mp4")
    filepath_05.touch()
    filepath_06 = Path(SUBFOLDER_PATH).joinpath("video06.mp4")
    filepath_06.touch()

    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(
        workflow.id, variables={"source": SCANFOLDER_PATH}
    )
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].input == {"source": "/tmp/madam_tests/scanfolder"}
    assert isinstance(instances[0].output, dict)
    assert instances[0].end_at is not None
    assert instances[0].status == JOB_STATUS_COMPLETE
    assert instances[0].error is None

    variables = instances[0].output
    assert str(filepath_01) in variables["items"]
    assert str(filepath_02) in variables["items"]
    assert str(filepath_03) in variables["items"]
    assert len(variables["items"]) == 3

    jobs = application.jobs.list(status="complete")
    assert len(jobs) == 1
    assert jobs[0].agent_id == "scanfolder_agent"
    assert jobs[0].agent_type == "scanfolder"
    assert jobs[0].input["source"] == SCANFOLDER_PATH
    assert jobs[0].headers["recursive"] == "false"
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_COMPLETE

    application.workflows.delete(workflow)


def test_scanfolder_recursive(application):
    """
    Test success on scanfolder agent

    :param application: Madam Application instance
    :return:
    """
    # create folders
    os.mkdir(SCANFOLDER_PATH)
    os.mkdir(SUBFOLDER_PATH)

    # create fake files
    filepath_01 = Path(SCANFOLDER_PATH).joinpath("video01.mp4")
    filepath_01.touch()
    filepath_02 = Path(SCANFOLDER_PATH).joinpath("video02.mp4")
    filepath_02.touch()
    filepath_03 = Path(SCANFOLDER_PATH).joinpath("video03.mp4")
    filepath_03.touch()
    filepath_04 = Path(SUBFOLDER_PATH).joinpath("video04.mp4")
    filepath_04.touch()
    filepath_05 = Path(SUBFOLDER_PATH).joinpath("video05.mp4")
    filepath_05.touch()
    filepath_06 = Path(SUBFOLDER_PATH).joinpath("video06.mp4")
    filepath_06.touch()

    with open(WORKFLOW_RECURSIVE_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(
        workflow.id, variables={"source": SCANFOLDER_PATH}
    )
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].input == {"source": "/tmp/madam_tests/scanfolder"}
    assert isinstance(instances[0].output, dict)
    assert instances[0].end_at is not None
    assert instances[0].status == JOB_STATUS_COMPLETE
    assert instances[0].error is None

    variables = instances[0].output
    assert str(filepath_01) in variables["items"]
    assert str(filepath_02) in variables["items"]
    assert str(filepath_03) in variables["items"]
    assert str(filepath_04) in variables["items"]
    assert str(filepath_05) in variables["items"]
    assert str(filepath_06) in variables["items"]
    assert len(variables["items"]) == 6

    jobs = application.jobs.list(status="complete")
    assert len(jobs) == 1
    assert jobs[0].agent_id == "scanfolder_agent"
    assert jobs[0].agent_type == "scanfolder"
    assert jobs[0].input["source"] == SCANFOLDER_PATH
    assert jobs[0].headers["recursive"] == "true"
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_COMPLETE

    application.workflows.delete(workflow)


def test_scanfolder_glob(application):
    """
    Test success on scanfolder agent

    :param application: Madam Application instance
    :return:
    """
    # create folders
    os.mkdir(SCANFOLDER_PATH)
    os.mkdir(SUBFOLDER_PATH)

    # create fake files
    filepath_01 = Path(SCANFOLDER_PATH).joinpath("video01.mp4")
    filepath_01.touch()
    filepath_02 = Path(SCANFOLDER_PATH).joinpath("video02.mp4")
    filepath_02.touch()
    filepath_03 = Path(SCANFOLDER_PATH).joinpath("video03.mp4")
    filepath_03.touch()
    filepath_04 = Path(SUBFOLDER_PATH).joinpath("video01.mp4")
    filepath_04.touch()
    filepath_05 = Path(SUBFOLDER_PATH).joinpath("video02.mp4")
    filepath_05.touch()
    filepath_06 = Path(SUBFOLDER_PATH).joinpath("video03.mp4")
    filepath_06.touch()

    with open(WORKFLOW_GLOB_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(
        workflow.id, variables={"source": SCANFOLDER_PATH}
    )
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].input == {"source": "/tmp/madam_tests/scanfolder"}
    assert isinstance(instances[0].output, dict)
    assert instances[0].end_at is not None
    assert instances[0].status == JOB_STATUS_COMPLETE
    assert instances[0].error is None

    variables = instances[0].output
    assert str(filepath_01) in variables["items"]
    assert str(filepath_02) in variables["items"]
    assert str(filepath_03) not in variables["items"]
    assert str(filepath_04) in variables["items"]
    assert str(filepath_05) in variables["items"]
    assert str(filepath_06) not in variables["items"]
    assert len(variables["items"]) == 4

    jobs = application.jobs.list(status="complete")
    assert len(jobs) == 1
    assert jobs[0].agent_id == "scanfolder_agent"
    assert jobs[0].agent_type == "scanfolder"
    assert jobs[0].input["source"] == SCANFOLDER_PATH
    assert jobs[0].headers["recursive"] == "true"
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_COMPLETE

    application.workflows.delete(workflow)
