# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on melt agent
"""
from pathlib import Path
from time import sleep

import jinja2

from madam.domains.entities.application import (
    STATUS_COMPLETE as APPLICATION_STATUS_COMPLETE,
)
from madam.domains.entities.job import STATUS_COMPLETE as JOB_STATUS_COMPLETE
from madam.domains.entities.workflow_instance import STATUS_COMPLETE, STATUS_RUNNING

WORKFLOW_ID = "melt_example"
WORKFLOW_PATH = Path(__file__).parent.parent.joinpath("assets/melt/melt.bpmn")
DESTINATION_PATH = "/tmp/madam_tests/melt_video_create.mp4"
ARGUMENTS = "frei0r.test_pat_B out=00:00:01:00 -track tone out=00:00:01:00 -consumer avformat:{{destination}}"
AGENT_ID = "melt_create_video"
AGENT_TYPE = "melt"


def test_melt(application):
    """
    Test success on melt agent

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_COMPLETE
    assert instances[0].input is None
    assert isinstance(instances[0].output, dict)
    assert instances[0].end_at is not None
    assert instances[0].error is None

    jobs = application.jobs.list(status=JOB_STATUS_COMPLETE)
    assert len(jobs) == 1
    assert jobs[0].agent_id == AGENT_ID
    assert jobs[0].agent_type == AGENT_TYPE
    assert jobs[0].headers["arguments"] == ARGUMENTS
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_COMPLETE

    applications = application.applications.list(status=APPLICATION_STATUS_COMPLETE)
    assert len(applications) == 1
    assert applications[0].job.id == jobs[0].id
    assert applications[0].name == "melt"
    # get command arguments jinja2 template
    template = jinja2.Template(jobs[0].headers["arguments"])
    assert applications[0].arguments == template.render(jobs[0].input)
    assert applications[0].end_at is not None
    assert applications[0].status == APPLICATION_STATUS_COMPLETE

    application.workflows.delete(workflow)
