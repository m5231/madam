# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on imagemagick agent
"""
from pathlib import Path
from time import sleep

import jinja2

from madam.domains.entities.application import (
    STATUS_COMPLETE as APPLICATION_STATUS_COMPLETE,
)
from madam.domains.entities.job import STATUS_COMPLETE as JOB_STATUS_COMPLETE
from madam.domains.entities.workflow_instance import STATUS_COMPLETE, STATUS_RUNNING

WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/imagemagick/imagemagick.bpmn"
)
WORKFLOW_COMPARE_PATH = Path(__file__).parent.parent.joinpath(
    "assets/imagemagick/imagemagick_compare.bpmn"
)
DESTINATION_PATH = "/tmp/madam_tests/imagemagick.png"
DESTINATION2_PATH = "/tmp/madam_tests/imagemagick2.png"
AGENT_TYPE = "imagemagick"


def test_imagemagick(application):
    """
    Test success on imagemagick agent

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_COMPLETE
    assert instances[0].input is None
    assert isinstance(instances[0].output, dict)
    assert instances[0].end_at is not None
    assert instances[0].error is None

    jobs = application.jobs.list(status=JOB_STATUS_COMPLETE)
    assert len(jobs) == 1
    assert jobs[0].agent_type == AGENT_TYPE
    assert jobs[0].headers["command"] == "convert"
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_COMPLETE

    applications = application.applications.list(status=APPLICATION_STATUS_COMPLETE)
    assert len(applications) == 1
    assert applications[0].job.id == jobs[0].id
    assert applications[0].name == "imagemagick"
    # get command arguments jinja2 template
    template = jinja2.Template(jobs[0].headers["arguments"])
    assert applications[0].arguments == template.render(jobs[0].input)
    assert applications[0].end_at is not None
    assert applications[0].status == APPLICATION_STATUS_COMPLETE

    assert Path(DESTINATION_PATH).exists()

    application.workflows.delete(workflow)


def test_imagemagick_compare(application):
    """
    Test success on imagemagick agent with compare command
    compare command exit with exit code
        0 if images are exactly the same
        1 if there is a difference (so we need to set ignore_failure=True in agent)
        2 if error

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_COMPARE_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_COMPLETE
    assert instances[0].input is None
    assert isinstance(instances[0].output, dict)
    assert instances[0].end_at is not None
    assert instances[0].error is None

    jobs = application.jobs.list(status=JOB_STATUS_COMPLETE)
    assert len(jobs) == 3
    assert jobs[2].agent_type == AGENT_TYPE
    assert jobs[2].headers["command"] == "compare"
    assert jobs[2].end_at is not None
    assert jobs[2].status == JOB_STATUS_COMPLETE

    applications = application.applications.list(status=APPLICATION_STATUS_COMPLETE)
    assert len(applications) == 3
    assert applications[2].job.id == jobs[2].id
    assert applications[2].name == "imagemagick"
    # get command arguments jinja2 template
    template = jinja2.Template(jobs[2].headers["arguments"])
    assert applications[2].arguments == template.render(jobs[2].input)
    assert applications[2].end_at is not None
    assert applications[2].status == APPLICATION_STATUS_COMPLETE

    assert Path(DESTINATION_PATH).exists()
    assert Path(DESTINATION2_PATH).exists()

    application.workflows.delete(workflow)
