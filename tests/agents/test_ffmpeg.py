# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on ffmpeg agent
"""
from pathlib import Path
from time import sleep

import jinja2
import yaml

from madam.adapters.repository.postgresql import PostgreSQLClient
from madam.domains.application import MainApplication
from madam.domains.entities.application import (
    STATUS_ABORTED as APPLICATION_STATUS_ABORTED,
)
from madam.domains.entities.application import (
    STATUS_COMPLETE as APPLICATION_STATUS_COMPLETE,
)
from madam.domains.entities.application import STATUS_CONTAINER_ERROR
from madam.domains.entities.application import STATUS_ERROR as APPLICATION_STATUS_ERROR
from madam.domains.entities.job import STATUS_ABORTED as JOB_STATUS_ABORTED
from madam.domains.entities.job import STATUS_COMPLETE as JOB_STATUS_COMPLETE
from madam.domains.entities.job import STATUS_ERROR as JOB_STATUS_ERROR
from madam.domains.entities.workflow_instance import (
    STATUS_ABORTED,
    STATUS_COMPLETE,
    STATUS_ERROR,
    STATUS_RUNNING,
)
from tests.conftest import TESTS_CONFIG_PATH

WORKFLOW_ID = "ffmpeg_example"
WORKFLOW_PATH = Path(__file__).parent.parent.joinpath("assets/ffmpeg/ffmpeg.bpmn")
WORKFLOW_PATH_BAD_ARGUMENTS = Path(__file__).parent.parent.joinpath(
    "assets/ffmpeg/ffmpeg_bad_arguments.bpmn"
)
WORKFLOW_PATH_LONG = Path(__file__).parent.parent.joinpath(
    "assets/ffmpeg/ffmpeg_long.bpmn"
)
DESTINATION_PATH = "/tmp/madam_tests/ffmpeg_video_create.mp4"
ARGUMENTS = "-y -f lavfi -i smptebars=duration=5:size=qcif:rate=25 {{destination}}"
BAD_ARGUMENTS = "-i /tmp/bad/path"
LONG_ARGUMENTS = (
    "-y -f lavfi -i smptebars=duration=360000000:size=qcif:rate=25 {{destination}}"
)
AGENT_ID = "ffmpeg_create_video"
AGENT_TYPE = "ffmpeg"


def test_ffmpeg(application):
    """
    Test success on ffmpeg agent

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_COMPLETE
    assert instances[0].input is None
    assert isinstance(instances[0].output, dict)
    assert instances[0].error is None
    assert instances[0].end_at is not None

    jobs = application.jobs.list(status=JOB_STATUS_COMPLETE)
    assert len(jobs) == 1
    assert jobs[0].agent_id == AGENT_ID
    assert jobs[0].agent_type == AGENT_TYPE
    assert jobs[0].headers["arguments"] == ARGUMENTS
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_COMPLETE

    applications = application.applications.list(status=APPLICATION_STATUS_COMPLETE)
    assert len(applications) == 1
    assert applications[0].job.id == jobs[0].id
    assert applications[0].name == "ffmpeg"
    # get command arguments jinja2 template
    template = jinja2.Template(jobs[0].headers["arguments"])
    assert applications[0].arguments == template.render(jobs[0].input)
    assert applications[0].end_at is not None
    assert applications[0].status == APPLICATION_STATUS_COMPLETE

    assert Path(DESTINATION_PATH).exists()
    # fixme: docker services should create non root files
    docker_config = application.config.get_root_key("docker")
    user_id, group_id = docker_config["user"].split(":")
    # file owner is not root
    assert Path(DESTINATION_PATH).stat().st_uid == int(user_id)
    # file group is not root
    assert Path(DESTINATION_PATH).stat().st_gid == int(group_id)

    application.workflows.delete(workflow)


def test_ffmpeg_container_rejected(tmp_folder):
    """
    Test error from container on ffmpeg agent

    :param tmp_folder: Tmp folder path fixture
    :return:
    """
    # load madam config
    with open(TESTS_CONFIG_PATH, encoding="utf-8") as filehandler:
        config = yaml.load(filehandler, Loader=yaml.FullLoader)

    config["docker"]["mounts"] = ["/path/to/source:/path/to/target"]

    bad_config_path = Path(tmp_folder).joinpath("madam_bad_config.yaml")

    with open(bad_config_path, "w", encoding="utf-8") as filehandler:
        yaml.safe_dump(config, filehandler)

    # create new Madam application domain
    application = MainApplication(str(bad_config_path))

    # cleanup tables by cascading on workflows
    database_client = PostgreSQLClient(application.config)
    database_client.clear("workflows")

    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    sleep(1)
    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_ERROR)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_ERROR
    assert instances[0].input is None
    assert instances[0].output is None
    assert instances[0].error is not None
    assert instances[0].end_at is not None

    jobs = application.jobs.list(status=JOB_STATUS_ERROR)
    assert len(jobs) == 1
    assert jobs[0].agent_id == AGENT_ID
    assert jobs[0].agent_type == AGENT_TYPE
    assert jobs[0].headers["arguments"] == ARGUMENTS
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_ERROR

    applications = application.applications.list(status=STATUS_CONTAINER_ERROR)
    assert len(applications) == 1
    assert applications[0].job.id == jobs[0].id
    assert applications[0].name == "ffmpeg"
    # get command arguments jinja2 template
    template = jinja2.Template(jobs[0].headers["arguments"])
    assert applications[0].arguments == template.render(jobs[0].input)
    assert applications[0].end_at is not None
    assert applications[0].status == STATUS_CONTAINER_ERROR

    application.workflows.delete(workflow)


def test_ffmpeg_application_failed(application):
    """
    Test error from application command in container on ffmpeg agent

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH_BAD_ARGUMENTS, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_ERROR)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_ERROR
    assert instances[0].input is None
    assert instances[0].output is None
    assert instances[0].error is not None
    assert instances[0].end_at is not None

    jobs = application.jobs.list(status=JOB_STATUS_ERROR)
    assert len(jobs) == 1
    assert jobs[0].agent_id == AGENT_ID
    assert jobs[0].agent_type == AGENT_TYPE
    assert jobs[0].headers["arguments"] == BAD_ARGUMENTS
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_ERROR

    applications = application.applications.list(status=APPLICATION_STATUS_ERROR)
    assert len(applications) == 1
    assert applications[0].job.id == jobs[0].id
    assert applications[0].name == "ffmpeg"
    # get command arguments jinja2 template
    template = jinja2.Template(jobs[0].headers["arguments"])
    assert applications[0].arguments == template.render(jobs[0].input)
    assert applications[0].end_at is not None
    assert applications[0].status == APPLICATION_STATUS_ERROR

    application.workflows.delete(workflow)


def test_ffmpeg_abort(application):
    """
    Test abort workflow instance with one long ffmpeg agent job

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH_LONG, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    instances = []
    while len(instances) < 1:
        instances = application.workflow_instances.list(status=STATUS_RUNNING)
        sleep(0.5)

    sleep(2)
    application.workflow_instances.abort(instances[0])

    instances = []
    while len(instances) < 1:
        instances = application.workflow_instances.list(status=STATUS_ABORTED)
        sleep(0.5)

    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_ABORTED
    assert instances[0].input is None
    assert instances[0].output is None
    assert instances[0].error is None
    assert instances[0].end_at is not None

    jobs = application.jobs.list(status=JOB_STATUS_ABORTED)
    assert len(jobs) == 1
    assert jobs[0].agent_id == AGENT_ID
    assert jobs[0].agent_type == AGENT_TYPE
    assert jobs[0].headers["arguments"] == LONG_ARGUMENTS
    assert jobs[0].end_at is not None
    assert jobs[0].status == JOB_STATUS_ABORTED

    applications = application.applications.list(status=APPLICATION_STATUS_ABORTED)
    assert len(applications) == 1
    assert applications[0].job.id == jobs[0].id
    assert applications[0].name == "ffmpeg"
    # get command arguments jinja2 template
    template = jinja2.Template(jobs[0].headers["arguments"])
    assert applications[0].arguments == template.render(jobs[0].input)
    assert applications[0].end_at is not None
    assert applications[0].status == APPLICATION_STATUS_ABORTED

    application.workflows.delete(workflow)
