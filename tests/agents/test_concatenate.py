# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on concatenate agent
"""
from pathlib import Path
from time import sleep

from madam.domains.entities.application import (
    STATUS_COMPLETE as APPLICATION_STATUS_COMPLETE,
)
from madam.domains.entities.job import STATUS_COMPLETE as JOB_STATUS_COMPLETE
from madam.domains.entities.workflow_instance import STATUS_COMPLETE, STATUS_RUNNING

WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/concatenate/concatenate.bpmn"
)
DESTINATION_PATH = "/tmp/madam_tests/concatenate.mp4"
AGENT_ID = "concatenate_agent"
AGENT_TYPE = "concatenate"


def test_concatenate(application):
    """
    Test success on concatenate agent

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_COMPLETE
    assert instances[0].input is None
    assert isinstance(instances[0].output, dict)
    assert instances[0].error is None
    assert instances[0].end_at is not None

    jobs = application.jobs.list(status=JOB_STATUS_COMPLETE)
    assert len(jobs) == 4
    assert jobs[3].agent_id == AGENT_ID
    assert jobs[3].agent_type == AGENT_TYPE
    assert jobs[3].end_at is not None
    assert jobs[3].status == JOB_STATUS_COMPLETE

    applications = application.applications.list(status=APPLICATION_STATUS_COMPLETE)
    assert len(applications) == 4
    assert applications[3].job.id == jobs[3].id
    assert applications[3].name == "ffmpeg"
    assert applications[3].end_at is not None
    assert applications[3].status == APPLICATION_STATUS_COMPLETE

    assert Path(DESTINATION_PATH).exists()

    application.workflows.delete(workflow)
