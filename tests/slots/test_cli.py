# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.
import sys
from datetime import datetime
from pathlib import Path
from typing import Optional
from urllib.error import URLError

import pytest
from ariadne import format_error, gql
from ariadne.graphql import handle_graphql_errors, parse_query, validate_query
from click.testing import CliRunner
from pytest_mock import MockerFixture

from madam.slots.cli import cli

WORKFLOW_PATH = Path(__file__).parents[1].joinpath("assets/workflows/test_create.bpmn")
runner = CliRunner()


@pytest.mark.parametrize(
    "command",
    [
        ["--endpoint", "localhost:5000", "workflow", "abort", "ID"],
        ["--endpoint", "localhost:5000", "workflow", "abort_instances", "ID"],
        ["--endpoint", "localhost:5000", "workflow", "clear_instances", "ID"],
        ["--endpoint", "localhost:5000", "workflow", "delete", "ID"],
        ["--endpoint", "localhost:5000", "workflow", "list"],
        ["--endpoint", "localhost:5000", "workflow", "upload", str(WORKFLOW_PATH)],
        ["--endpoint", "localhost:5000", "workflow", "show", "ID"],
        ["--endpoint", "localhost:5000", "workflow", "start", "ID"],
        ["--endpoint", "localhost:5000", "timer", "abort", "ID"],
        ["--endpoint", "localhost:5000", "timer", "delete", "ID"],
        ["--endpoint", "localhost:5000", "timer", "list"],
        ["--endpoint", "localhost:5000", "instance", "abort", "ID"],
        ["--endpoint", "localhost:5000", "instance", "delete", "ID"],
        ["--endpoint", "localhost:5000", "instance", "list"],
        ["--endpoint", "localhost:5000", "instance", "show", "ID"],
        ["--endpoint", "localhost:5000", "job", "list"],
        ["--endpoint", "localhost:5000", "watchfolder", "create", '{"path": "dummy"}'],
        [
            "--endpoint",
            "localhost:5000",
            "watchfolder",
            "update",
            '{"id": "UUID", "path": "dummy"}',
        ],
        ["--endpoint", "localhost:5000", "watchfolder", "start", "UUID"],
        ["--endpoint", "localhost:5000", "watchfolder", "stop", "UUID"],
        ["--endpoint", "localhost:5000", "watchfolder", "delete", "UUID"],
        ["--endpoint", "localhost:5000", "watchfolder", "show", "UUID"],
        ["--endpoint", "localhost:5000", "watchfolder", "list"],
    ],
)
def test_query_syntax(graphql_schema, mocker: MockerFixture, command):
    """
    test_command_error

    :param mocker: MockerFixture
    :param command: cli command
    :return:
    """
    # validate query
    def query_endpoint(
        query: str, url: str, variables: Optional[dict] = None
    ):  # pylint: disable=unused-argument
        """
        Validate query to send

        :param query: GraphQL Query
        :param url: MADAM server Url
        :param variables: Variables of the mutation/query
        :return:
        """
        # check query syntax
        query = gql(query)
        # create query document
        document = parse_query(query)
        # validate query document against schema
        validation_errors = validate_query(graphql_schema, document)
        if validation_errors:
            return handle_graphql_errors(
                validation_errors, logger=None, error_formatter=format_error, debug=True
            )
        sys.exit(0)
        return None

    query_endpoint_mock = mocker.patch("madam.slots.cli.query_endpoint")
    query_endpoint_mock.side_effect = query_endpoint
    runner.invoke(cli, command, catch_exceptions=False)


def test_query_endpoint_errors(mocker: MockerFixture):
    """
    test_query_endpoint_errors

    :param mocker: MockerFixture
    :return:
    """
    # command has no importance here, just testing cli.query_endpoint() errors
    command = ["--endpoint", "localhost:5000", "watchfolder", "list"]
    urllib_exception_message = "test urllib exception"

    # mock http request
    mocker.patch(
        "madam.slots.cli.HTTPClient.request",
        side_effect=URLError(urllib_exception_message),
    )

    result = runner.invoke(cli, command, catch_exceptions=False)
    try:
        assert result.exit_code == 1
    except AssertionError as exception:
        print(result.output)
        raise AssertionError from exception
    assert f"Error: <urlopen error {urllib_exception_message}>\n" in result.output

    # test GrapQL errors
    httpclient_request_mock = mocker.patch("madam.libs.http.HTTPClient.request")
    httpclient_request_mock.return_value = {
        "errors": [{"path": "$.content", "message": "missing variable content"}]
    }

    result = runner.invoke(cli, command, catch_exceptions=False)
    assert result.exit_code == 1
    assert (
        f"{httpclient_request_mock.return_value['errors'][0]['message']} \
({httpclient_request_mock.return_value['errors'][0]['path']})"
        in result.output
    )


@pytest.mark.parametrize(
    "command_response",
    [
        (
            ["--endpoint", "localhost:5000", "workflow", "abort", "ID"],
            {
                "data": {
                    "abortWorkflow": {
                        "status": True,
                        "errors": [],
                    }
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "workflow", "abort_instances", "ID"],
            {
                "data": {
                    "abortWorkflowInstancesByWorkflow": {"status": True, "errors": []}
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "workflow", "clear_instances", "ID"],
            {
                "data": {
                    "deleteWorkflowInstancesByWorkflow": {"status": True, "errors": []}
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "workflow", "delete", "ID"],
            {"data": {"deleteWorkflow": {"status": True, "errors": []}}},
        ),
        (
            ["--endpoint", "localhost:5000", "workflow", "upload", str(WORKFLOW_PATH)],
            {
                "data": {
                    "createWorkflow": {
                        "status": True,
                        "errors": [],
                        "workflow": {"id": "wf_id", "name": "wf_name", "version": 1},
                    }
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "workflow", "start", "ID"],
            {"data": {"startWorkflow": {"status": True, "errors": []}}},
        ),
        (
            ["--endpoint", "localhost:5000", "timer", "abort", "ID"],
            {"data": {"abortTimer": {"status": True, "errors": []}}},
        ),
        (
            ["--endpoint", "localhost:5000", "timer", "delete", "ID"],
            {"data": {"deleteTimer": {"status": True, "errors": []}}},
        ),
        (
            ["--endpoint", "localhost:5000", "instance", "abort", "ID"],
            {"data": {"abortWorkflowInstance": {"status": True, "errors": []}}},
        ),
        (
            ["--endpoint", "localhost:5000", "instance", "delete", "ID"],
            {"data": {"deleteWorkflowInstance": {"status": True, "errors": []}}},
        ),
        (
            ["--endpoint", "localhost:5000", "watchfolder", "create"],
            {
                "data": {
                    "createWatchfolder": {
                        "status": True,
                        "errors": [],
                        "watchfolder": {"id": "watch_id", "path": "dummy"},
                    }
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "watchfolder", "update"],
            {
                "data": {
                    "updateWatchfolder": {
                        "status": True,
                        "errors": [],
                        "watchfolder": {"id": "watch_id", "path": "dummy"},
                    }
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "watchfolder", "start", "UUID"],
            {"data": {"startWatchfolder": {"status": True, "errors": []}}},
        ),
        (
            ["--endpoint", "localhost:5000", "watchfolder", "stop", "UUID"],
            {"data": {"stopWatchfolder": {"status": True, "errors": []}}},
        ),
        (
            ["--endpoint", "localhost:5000", "watchfolder", "delete", "UUID"],
            {"data": {"deleteWatchfolder": {"status": True, "errors": []}}},
        ),
    ],
)
def test_mutation(mocker: MockerFixture, command_response):
    """
    test_mutation

    :param mocker: MockerFixture
    :return:
    """
    httpclient_request_mock = mocker.patch("madam.libs.http.HTTPClient.request")
    httpclient_request_mock.return_value = command_response[1]

    input_ = None
    # mock watchfolder stdin input
    if "watchfolder" in command_response[0]:
        if "create" in command_response[0]:
            input_ = '{"path": "/tmp", "added_workflow": {"id": "WORKFLOW_ID"}}'
        if "update" in command_response[0]:
            input_ = '{"id": "xxxxxxx", "path": "/tmp", "added_workflow": {"id": "WORKFLOW_ID"}}'

    result = runner.invoke(
        cli, command_response[0], input=input_, catch_exceptions=False
    )

    try:
        assert result.exit_code == 0
    except AssertionError as exception:
        print(result.output)
        raise AssertionError from exception


@pytest.mark.parametrize(
    "command_response",
    [
        (
            ["--endpoint", "localhost:5000", "workflow", "list"],
            {
                "data": {
                    "workflows": {
                        "count": 2,
                        "result": [
                            {
                                "id": "id1",
                                "version": 1,
                                "name": "n1",
                                "timer": None,
                                "created_at": datetime.now().isoformat(),
                            },
                            {
                                "id": "id2",
                                "version": 1,
                                "name": "n2",
                                "timer": None,
                                "created_at": datetime.now().isoformat(),
                            },
                        ],
                    }
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "timer", "list"],
            {
                "data": {
                    "timers": {
                        "count": 2,
                        "result": [
                            {
                                "id": "id1",
                                "workflow": {
                                    "id": "wf_id",
                                    "version": 1,
                                },
                                "status": "running",
                                "start_at": datetime.now().isoformat(),
                                "end_at": datetime.now().isoformat(),
                                "input": {"test": "one"},
                            },
                            {
                                "id": "id2",
                                "workflow": {
                                    "id": "wf_id",
                                    "version": 1,
                                },
                                "status": "running",
                                "start_at": datetime.now().isoformat(),
                                "end_at": datetime.now().isoformat(),
                                "input": {"test": "two"},
                            },
                        ],
                    }
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "instance", "list"],
            {
                "data": {
                    "workflow_instances": {
                        "count": 2,
                        "result": [
                            {
                                "id": "id1",
                                "workflow": {
                                    "id": "wf_id",
                                    "version": 1,
                                },
                                "status": "running",
                                "start_at": datetime.now().isoformat(),
                                "end_at": datetime.now().isoformat(),
                            },
                            {
                                "id": "id2",
                                "workflow": {
                                    "id": "wf_id",
                                    "version": 1,
                                },
                                "status": "running",
                                "start_at": datetime.now().isoformat(),
                                "end_at": datetime.now().isoformat(),
                            },
                        ],
                    }
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "job", "list"],
            {
                "data": {
                    "jobs": {
                        "count": 2,
                        "result": [
                            {
                                "id": "id1",
                                "workflow_instance": {
                                    "id": "wi_id",
                                    "workflow": {
                                        "id": "wf_id",
                                        "version": 1,
                                    },
                                },
                                "status": "running",
                                "start_at": datetime.now().isoformat(),
                                "end_at": datetime.now().isoformat(),
                                "error": None,
                            },
                            {
                                "id": "id2",
                                "workflow_instance": {
                                    "id": "wi_id",
                                    "workflow": {
                                        "id": "wf_id",
                                        "version": 1,
                                    },
                                },
                                "status": "running",
                                "start_at": datetime.now().isoformat(),
                                "end_at": datetime.now().isoformat(),
                                "error": None,
                            },
                        ],
                    }
                }
            },
        ),
        (
            ["--endpoint", "localhost:5000", "watchfolder", "list"],
            {
                "data": {
                    "watchfolders": {
                        "count": 2,
                        "result": [
                            {
                                "id": "id1",
                                "path": "dummy",
                                "start_at": datetime.now().isoformat(),
                                "end_at": datetime.now().isoformat(),
                                "status": "running",
                                "added_workflow": {
                                    "id": "wf_id",
                                    "version": 1,
                                },
                                "modified_workflow": {
                                    "id": "wf_id",
                                    "version": 1,
                                },
                                "deleted_workflow": {
                                    "id": "wf_id",
                                    "version": 1,
                                },
                            },
                            {
                                "id": "id2",
                                "path": "dummy",
                                "start_at": None,
                                "end_at": None,
                                "status": "stopped",
                                "added_workflow": None,
                                "modified_workflow": None,
                                "deleted_workflow": None,
                            },
                        ],
                    }
                }
            },
        ),
    ],
)
def test_query(mocker: MockerFixture, command_response):
    """
    test_mutation

    :param mocker: MockerFixture
    :return:
    """
    httpclient_request_mock = mocker.patch("madam.libs.http.HTTPClient.request")
    httpclient_request_mock.return_value = command_response[1]

    result = runner.invoke(cli, command_response[0], catch_exceptions=False)
    assert result.exit_code == 0


def test_workflow_show(mocker: MockerFixture):
    """
    test_workflow_show

    :param mocker: MockerFixture
    :return:
    """
    httpclient_request_mock = mocker.patch("madam.libs.http.HTTPClient.request")
    httpclient_request_mock.side_effect = [
        {
            "data": {
                "workflow": {
                    "id": "id1",
                    "version": 1,
                    "name": "n1",
                    "sha256": "xxx",
                    "timer": "P5SR2",
                    "created_at": datetime.now().isoformat(),
                }
            }
        },
        {
            "data": {
                "timers": {
                    "count": 1,
                    "result": [
                        {
                            "id": "id1",
                            "status": "running",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                            "input": {"input_var": "dummy"},
                        }
                    ],
                }
            }
        },
        {
            "data": {
                "workflow_instances": {
                    "count": 2,
                    "result": [
                        {
                            "id": "id1",
                            "workflow": {
                                "id": "wf_id",
                                "version": 1,
                            },
                            "status": "running",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                            "input": {"input_var": "dummy"},
                            "output": {"output_var": "dummy"},
                        },
                        {
                            "id": "id2",
                            "workflow": {
                                "id": "wf_id",
                                "version": 1,
                            },
                            "status": "running",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                            "input": {"input_var": "dummy"},
                            "output": {"output_var": "dummy"},
                        },
                    ],
                }
            }
        },
    ]

    result = runner.invoke(
        cli,
        ["--endpoint", "localhost:5000", "workflow", "show", "ID"],
        catch_exceptions=False,
    )
    assert result.exit_code == 0


def test_instance_show(mocker: MockerFixture):
    """
    test_instance_show

    :param mocker: MockerFixture
    :return:
    """
    httpclient_request_mock = mocker.patch("madam.libs.http.HTTPClient.request")
    httpclient_request_mock.side_effect = [
        {
            "data": {
                "workflow_instance": {
                    "id": "id1",
                    "workflow": {"id": "wf_id", "version": 1},
                    "status": "running",
                    "start_at": datetime.now().isoformat(),
                    "end_at": datetime.now().isoformat(),
                    "input": {"input_var": "dummy"},
                    "output": {"output_var": "dummy"},
                    "error": None,
                }
            }
        },
        {
            "data": {
                "jobs": {
                    "count": 2,
                    "result": [
                        {
                            "id": "id1",
                            "status": "running",
                            "agent_id": "ffmpeg_create_video",
                            "agent_type": "ffmpeg",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                            "error": None,
                        },
                        {
                            "id": "id2",
                            "status": "running",
                            "agent_id": "ffmpeg_create_video",
                            "agent_type": "ffmpeg",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                            "error": None,
                        },
                    ],
                }
            }
        },
        {
            "data": {
                "jobs": {
                    "count": 2,
                    "result": [
                        {
                            "id": "id1",
                            "status": "running",
                            "agent_id": "ffmpeg_create_video",
                            "agent_type": "ffmpeg",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                            "error": None,
                            "headers": {"header_key": "dummy"},
                            "input": {"input_var": "dummy"},
                            "output": {"output_var": "dummy"},
                        },
                        {
                            "id": "id2",
                            "status": "running",
                            "agent_id": "ffmpeg_create_video",
                            "agent_type": "ffmpeg",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                            "error": None,
                            "headers": {"header_key": "dummy"},
                            "input": {"input_var": "dummy"},
                            "output": {"output_var": "dummy"},
                        },
                    ],
                }
            }
        },
        {
            "data": {
                "applications": {
                    "count": 2,
                    "result": [
                        {
                            "id": "id1",
                            "name": "ffmpeg",
                            "status": "error",
                            "arguments": "-y -i filepath",
                            "logs": "logs",
                            "container_id": "xxxx",
                            "container_name": "dummy",
                            "container_error": "error message",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                        },
                        {
                            "id": "id2",
                            "name": "ffmpeg",
                            "status": "error",
                            "arguments": "-y -i filepath",
                            "logs": "logs",
                            "container_id": "yyy",
                            "container_name": "dummy 2",
                            "container_error": "error message 2",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                        },
                    ],
                }
            }
        },
        {
            "data": {
                "applications": {
                    "count": 2,
                    "result": [
                        {
                            "id": "id1",
                            "name": "ffmpeg",
                            "status": "error",
                            "arguments": "-y -i filepath",
                            "logs": "logs",
                            "container_id": "xxxx",
                            "container_name": "dummy",
                            "container_error": "error message",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                        },
                        {
                            "id": "id2",
                            "name": "ffmpeg",
                            "status": "error",
                            "arguments": "-y -i filepath",
                            "logs": "logs",
                            "container_id": "yyy",
                            "container_name": "dummy 2",
                            "container_error": "error message 2",
                            "start_at": datetime.now().isoformat(),
                            "end_at": datetime.now().isoformat(),
                        },
                    ],
                }
            }
        },
    ]

    result = runner.invoke(
        cli,
        ["--endpoint", "localhost:5000", "instance", "show", "ID"],
        catch_exceptions=False,
    )
    assert result.exit_code == 0


def test_watchfolder_show(mocker: MockerFixture):
    """
    test_watchfolder_show

    :param mocker: MockerFixture
    :return:
    """
    httpclient_request_mock = mocker.patch("madam.libs.http.HTTPClient.request")
    httpclient_request_mock.side_effect = [
        {
            "data": {
                "watchfolder": {
                    "id": "ID",
                    "path": "dummy",
                    "start_at": datetime.now().isoformat(),
                    "end_at": datetime.now().isoformat(),
                    "status": "stopped",
                    "re_files": "dummy",
                    "re_dirs": "dummy",
                    "added_workflow": {"id": "dummy", "version": 1},
                    "added_variables": {"var": "dummy"},
                    "modified_workflow": {"id": "dummy", "version": 1},
                    "modified_variables": {"var": "dummy"},
                    "deleted_workflow": {"id": "dummy", "version": 1},
                    "deleted_variables": {"var": "dummy"},
                    "added_items_key": "dummy",
                    "modified_items_key": "dummy",
                    "deleted_items_key": "dummy",
                }
            }
        },
    ]

    result = runner.invoke(
        cli,
        ["--endpoint", "localhost:5000", "watchfolder", "show", "ID"],
        catch_exceptions=False,
    )
    assert result.exit_code == 0
