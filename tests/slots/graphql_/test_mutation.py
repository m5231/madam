# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

import json
from datetime import datetime
from pathlib import Path
from time import sleep
from typing import List

import pytest
from ariadne import graphql_sync

from madam.domains.application import MainApplication
from madam.domains.entities.application import (
    STATUS_ABORTED as APPLICATION_STATUS_ABORTED,
)
from madam.domains.entities.job import STATUS_ABORTED as JOB_STATUS_ABORTED
from madam.domains.entities.job import STATUS_COMPLETE as JOB_STATUS_COMPLETE
from madam.domains.entities.timer import STATUS_ABORTED as TIMER_STATUS_ABORTED
from madam.domains.entities.timer import CyclicTimer
from madam.domains.entities.watchfolder import (
    STATUS_RUNNING as WATCHFOLDER_STATUS_RUNNING,
)
from madam.domains.entities.watchfolder import (
    STATUS_STOPPED as WATCHFOLDER_STATUS_STOPPED,
)
from madam.domains.entities.workflow_instance import (
    STATUS_ABORTED as INSTANCE_STATUS_ABORTED,
)
from madam.domains.entities.workflow_instance import (
    STATUS_COMPLETE as INSTANCE_STATUS_COMPLETE,
)
from madam.domains.entities.workflow_instance import (
    STATUS_RUNNING as INSTANCE_STATUS_RUNNING,
)
from madam.domains.entities.workflow_instance import WorkflowInstance

WORKFLOW_PATH = Path(__file__).parents[2].joinpath("assets/workflows/test_create.bpmn")
WORKFLOW_PATH_LONG = (
    Path(__file__).parents[2].joinpath("assets/ffmpeg/ffmpeg_long.bpmn")
)
WORKFLOW_CYCLIC_TIMER_PATH = (
    Path(__file__).parents[2].joinpath("assets/workflows/cyclic_timer.bpmn")
)


def test_create_workflow(application: MainApplication, graphql_schema):
    """
    test_create_workflow

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    mutation_ = """
mutation ($content: String!) {
    createWorkflow(content: $content) {
        status
        error
        workflow {
            id
            version
            sha256
            }
    }
}
    """

    data = {
        "operationName": None,
        "variables": {"content": workflow_content},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert response["data"]["createWorkflow"]["status"] is True
    assert response["data"]["createWorkflow"]["error"] is None
    assert response["data"]["createWorkflow"]["workflow"] == {
        "id": "test_create_id",
        "sha256": "5d0838d15fa8c7035bd352ad7dff9e46c95edd6dda9a18f522976ce28b911aa1",
        "version": 1,
    }
    workflow = application.workflows.read(
        response["data"]["createWorkflow"]["workflow"]["id"]
    )
    application.workflows.delete(workflow)


def test_delete_workflow(application: MainApplication, graphql_schema):
    """
    test_delete_workflow

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.read(workflow.id) is not None

    mutation_ = """
mutation ($id: String!) {
    deleteWorkflow(id: $id) {
        status
        error
    }
}
    """

    data = {
        "operationName": None,
        "variables": {"id": workflow.id},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert response["data"]["deleteWorkflow"]["status"] is True
    assert response["data"]["deleteWorkflow"]["error"] is None

    with pytest.raises(Exception):
        application.workflows.read(workflow.id)


def test_start_workflow(application: MainApplication, graphql_schema):
    """
    test_start_workflow

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.read(workflow.id) is not None

    mutation_ = """
    mutation ($id: String!, $version: Int, $variables: JSON) {
        startWorkflow(id: $id, version: $version, variables: $variables) {
            status
            error
        }
    }
        """

    data = {
        "operationName": None,
        "variables": {
            "id": workflow.id,
            "version": None,
            "variables": json.dumps({"source": "/tmp"}),
        },
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["startWorkflow"]["status"] is True
    assert response["data"]["startWorkflow"]["error"] is None

    while (
        len(
            application.workflow_instances.list(
                workflow=workflow, status=INSTANCE_STATUS_RUNNING
            )
        )
        == 1
    ):
        sleep(0.5)

    instances = application.workflow_instances.list(
        workflow=workflow, status=INSTANCE_STATUS_COMPLETE
    )
    assert len(instances) == 1

    application.workflows.delete(workflow)


def test_abort_workflow(application: MainApplication, graphql_schema):
    """
    test_abort_workflow

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH_LONG, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    instances: List[WorkflowInstance] = []
    while len(instances) < 1:
        instances = application.workflow_instances.list(status=INSTANCE_STATUS_RUNNING)
        sleep(0.5)

    sleep(2)
    mutation_ = """
    mutation ($id: String!, $version: Int) {
        abortWorkflow(id: $id, version: $version) {
            status
            error
        }
    }
        """

    data = {
        "operationName": None,
        "variables": {"id": workflow.id, "version": None},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["abortWorkflow"]["status"] is True
    assert response["data"]["abortWorkflow"]["error"] is None

    instances = []
    while len(instances) < 1:
        instances = application.workflow_instances.list(status=INSTANCE_STATUS_ABORTED)
        sleep(0.5)

    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == INSTANCE_STATUS_ABORTED
    assert instances[0].input is None
    assert instances[0].output is None
    assert instances[0].end_at is not None

    jobs = application.jobs.list(status=JOB_STATUS_ABORTED)
    assert len(jobs) == 1
    assert jobs[0].status == JOB_STATUS_ABORTED

    applications = application.applications.list(status=APPLICATION_STATUS_ABORTED)
    assert len(applications) == 1
    assert applications[0].status == APPLICATION_STATUS_ABORTED

    application.workflows.delete(workflow)


def test_abort_timer(application: MainApplication, graphql_schema):
    """
    test_abort_timer

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_CYCLIC_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id, variables={"source": "/tmp"})
    assert result is True

    sleep(1)
    timers = application.timers.list()

    mutation_ = """
    mutation ($id: ID!) {
        abortTimer(id: $id) {
            status
            error
        }
    }
    """
    data = {
        "operationName": None,
        "variables": {"id": str(timers[0].id)},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["abortTimer"]["status"] is True
    assert response["data"]["abortTimer"]["error"] is None

    timers = []
    while len(timers) < 1:
        timers = application.timers.list(status=TIMER_STATUS_ABORTED)
        sleep(0.5)

    assert len(timers) == 1
    assert isinstance(timers[0], CyclicTimer)
    assert timers[0].repeat == 2
    assert timers[0].interval.seconds == 1
    assert timers[0].status == TIMER_STATUS_ABORTED
    assert timers[0].end_at is not None

    jobs = application.jobs.list()
    assert len(jobs) == 1
    assert jobs[0].status == JOB_STATUS_COMPLETE

    application.workflows.delete(workflow)


def test_delete_timer(application: MainApplication, graphql_schema):
    """
    test_delete_timer

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_CYCLIC_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    now_ = datetime.now()
    timer = application.timers.create_date_timer(workflow, now_, {"source": "/tmp"})
    timers = application.timers.list()
    assert len(timers) == 1

    mutation_ = """
    mutation ($id: ID!) {
        deleteTimer(id: $id) {
            status
            error
        }
    }
    """
    data = {
        "operationName": None,
        "variables": {"id": str(timer.id)},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["deleteTimer"]["status"] is True
    assert response["data"]["deleteTimer"]["error"] is None

    timers = application.timers.list()
    assert len(timers) == 0

    application.workflows.delete(workflow)


def test_abort_workflow_instance(application: MainApplication, graphql_schema):
    """
    test_abort_workflow_instance

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH_LONG, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    instances: List[WorkflowInstance] = []
    while len(instances) < 1:
        instances = application.workflow_instances.list(status=INSTANCE_STATUS_RUNNING)
        sleep(0.5)

    sleep(2)
    mutation_ = """
    mutation ($id: ID!) {
        abortWorkflowInstance(id: $id) {
            status
            error
        }
    }
        """

    data = {
        "operationName": None,
        "variables": {"id": str(instances[0].id)},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["abortWorkflowInstance"]["status"] is True
    assert response["data"]["abortWorkflowInstance"]["error"] is None

    instances = []
    while len(instances) < 1:
        instances = application.workflow_instances.list(status=INSTANCE_STATUS_ABORTED)
        sleep(0.5)

    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == INSTANCE_STATUS_ABORTED
    assert instances[0].input is None
    assert instances[0].output is None
    assert instances[0].end_at is not None

    jobs = application.jobs.list(status=JOB_STATUS_ABORTED)
    assert len(jobs) == 1
    assert jobs[0].status == JOB_STATUS_ABORTED

    applications = application.applications.list(status=APPLICATION_STATUS_ABORTED)
    assert len(applications) == 1
    assert applications[0].status == APPLICATION_STATUS_ABORTED

    application.workflows.delete(workflow)


def test_delete_workflow_instance(application: MainApplication, graphql_schema):
    """
    test_delete_workflow_instance

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.read(workflow.id) is not None

    instance = application.workflow_instances.create(workflow)
    assert application.workflow_instances.read(instance.id) is not None

    mutation_ = """
mutation ($id: ID!) {
    deleteWorkflowInstance(id: $id) {
        status
        error
    }
}
    """

    data = {
        "operationName": None,
        "variables": {"id": str(instance.id)},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert response["data"]["deleteWorkflowInstance"]["status"] is True
    assert response["data"]["deleteWorkflowInstance"]["error"] is None

    with pytest.raises(Exception):
        application.workflow_instances.read(instance.id)


def test_abort_workflow_instances_by_workflow(
    application: MainApplication, graphql_schema
):
    """
    test_abort_workflow_instances_by_workflow

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.read(workflow.id) is not None

    instance_01 = application.workflow_instances.create(workflow)
    assert application.workflow_instances.read(instance_01.id) is not None
    instance_02 = application.workflow_instances.create(workflow)
    assert application.workflow_instances.read(instance_02.id) is not None
    mutation_ = """
mutation ($id: String!, $version: Int) {
    abortWorkflowInstancesByWorkflow(id: $id, version: $version) {
        status
        error
    }
}
    """

    data = {
        "operationName": None,
        "variables": {"id": workflow.id, "version": 1},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert response["data"]["abortWorkflowInstancesByWorkflow"]["status"] is True
    assert response["data"]["abortWorkflowInstancesByWorkflow"]["error"] is None

    new_instance_01 = application.workflow_instances.read(instance_01.id)
    new_instance_02 = application.workflow_instances.read(instance_02.id)

    assert new_instance_01.status == INSTANCE_STATUS_ABORTED
    assert new_instance_02.status == INSTANCE_STATUS_ABORTED

    application.workflows.delete(workflow)


def test_delete_workflow_instances_by_workflow(
    application: MainApplication, graphql_schema
):
    """
    test_delete_workflow_instances_by_workflow

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.read(workflow.id) is not None

    instance_01 = application.workflow_instances.create(workflow)
    assert application.workflow_instances.read(instance_01.id) is not None
    instance_02 = application.workflow_instances.create(workflow)
    assert application.workflow_instances.read(instance_02.id) is not None
    mutation_ = """
mutation ($id: String!, $version: Int) {
    deleteWorkflowInstancesByWorkflow(id: $id, version: $version) {
        status
        error
    }
}
    """

    data = {
        "operationName": None,
        "variables": {"id": workflow.id, "version": 1},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert response["data"]["deleteWorkflowInstancesByWorkflow"]["status"] is True
    assert response["data"]["deleteWorkflowInstancesByWorkflow"]["error"] is None

    with pytest.raises(Exception):
        application.workflow_instances.read(instance_01.id)
    with pytest.raises(Exception):
        application.workflow_instances.read(instance_02.id)

    application.workflows.delete(workflow)


def test_create_watchfolder(application: MainApplication, graphql_schema):
    """
    test_create_watchfolder

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    folder_path = "/tmp/madam_tests"
    re_files = r".+\.mp4"
    re_dirs = r"sources_.+"
    variables = {"var1": 1}
    items_key = "items"

    mutation_ = """
mutation (
    $path: String!,
    $re_files: String!,
    $re_dirs: String!,
    $added_workflow: WorkflowInput!,
    $added_variables: JSON!
    $modified_workflow: WorkflowInput!,
    $modified_variables: JSON!
    $deleted_workflow: WorkflowInput!,
    $deleted_variables: JSON!,
    $added_items_key: String!,
    $modified_items_key: String!,
    $deleted_items_key: String!
) {
    createWatchfolder(
        path: $path,
        re_files: $re_files,
        re_dirs: $re_dirs,
        added_workflow: $added_workflow,
        added_variables: $added_variables
        modified_workflow: $modified_workflow,
        modified_variables: $modified_variables
        deleted_workflow: $deleted_workflow,
        deleted_variables: $deleted_variables,
        added_items_key: $added_items_key,
        modified_items_key: $modified_items_key,
        deleted_items_key: $deleted_items_key
    ) {
        status
        error
        watchfolder {
            id
            path
            re_files
            re_dirs
            added_workflow {
                    id
                }
            added_variables
            modified_workflow {
                    id
                }
            modified_variables
            deleted_workflow {
                    id
                }
            deleted_variables
            added_items_key
            modified_items_key
            deleted_items_key
            }
    }
}
    """

    data = {
        "operationName": None,
        "variables": {
            "path": folder_path,
            "re_files": re_files,
            "re_dirs": re_dirs,
            "added_workflow": {"id": workflow.id},
            "added_variables": variables,
            "modified_workflow": {"id": workflow.id},
            "modified_variables": variables,
            "deleted_workflow": {"id": workflow.id},
            "deleted_variables": variables,
            "added_items_key": items_key,
            "modified_items_key": items_key,
            "deleted_items_key": items_key,
        },
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert response["data"]["createWatchfolder"]["status"] is True
    assert response["data"]["createWatchfolder"]["error"] is None
    assert response["data"]["createWatchfolder"]["watchfolder"]["path"] == folder_path
    assert response["data"]["createWatchfolder"]["watchfolder"]["re_files"] == re_files
    assert response["data"]["createWatchfolder"]["watchfolder"]["re_dirs"] == re_dirs
    assert (
        response["data"]["createWatchfolder"]["watchfolder"]["added_workflow"]["id"]
        == workflow.id
    )
    assert (
        response["data"]["createWatchfolder"]["watchfolder"]["added_variables"]
        == variables
    )
    assert (
        response["data"]["createWatchfolder"]["watchfolder"]["modified_workflow"]["id"]
        == workflow.id
    )
    assert (
        response["data"]["createWatchfolder"]["watchfolder"]["modified_variables"]
        == variables
    )
    assert (
        response["data"]["createWatchfolder"]["watchfolder"]["deleted_workflow"]["id"]
        == workflow.id
    )
    assert (
        response["data"]["createWatchfolder"]["watchfolder"]["deleted_variables"]
        == variables
    )
    assert (
        response["data"]["createWatchfolder"]["watchfolder"]["added_items_key"]
        == items_key
    )
    assert (
        response["data"]["createWatchfolder"]["watchfolder"]["modified_items_key"]
        == items_key
    )
    assert (
        response["data"]["createWatchfolder"]["watchfolder"]["deleted_items_key"]
        == items_key
    )

    watchfolder = application.watchfolders.read(
        response["data"]["createWatchfolder"]["watchfolder"]["id"]
    )
    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_update_watchfolder(application: MainApplication, graphql_schema):
    """
    test_update_watchfolder

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.read(workflow.id) is not None

    variables = {"source": "/tmp"}
    watchfolder = application.watchfolders.create(
        "/tmp/madam_tests",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
    )

    assert watchfolder.start_at is None
    assert watchfolder.status == WATCHFOLDER_STATUS_STOPPED

    updated_path = "/tmp"
    updated_re_files = r".*\.mp4"
    updated_re_dirs = r"sources_.*"
    mutation_ = """
mutation ($id: ID!, $path: String!, $re_files: String, $re_dirs: String!) {
    updateWatchfolder(
    id: $id,
    path: $path,
    re_files: $re_files,
    re_dirs: $re_dirs
    ) {
        status
        error
        watchfolder {
            id
            path
            re_files
            re_dirs
        }
    }
}
    """

    data = {
        "operationName": None,
        "variables": {
            "id": str(watchfolder.id),
            "path": updated_path,
            "re_files": updated_re_files,
            "re_dirs": updated_re_dirs,
        },
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["updateWatchfolder"]["status"] is True
    assert response["data"]["updateWatchfolder"]["error"] is None
    assert response["data"]["updateWatchfolder"]["watchfolder"]["id"] == str(
        watchfolder.id
    )
    assert response["data"]["updateWatchfolder"]["watchfolder"]["path"] == updated_path
    assert (
        response["data"]["updateWatchfolder"]["watchfolder"]["re_files"]
        == updated_re_files
    )
    assert (
        response["data"]["updateWatchfolder"]["watchfolder"]["re_dirs"]
        == updated_re_dirs
    )

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_start_watchfolder(application: MainApplication, graphql_schema):
    """
    test_start_watchfolder

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.read(workflow.id) is not None

    variables = {"source": "/tmp"}
    watchfolder = application.watchfolders.create(
        "/tmp/madam_tests",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
    )

    assert watchfolder.start_at is None
    assert watchfolder.status == WATCHFOLDER_STATUS_STOPPED

    mutation_ = """
mutation ($id: ID!) {
    startWatchfolder(id: $id) {
        status
        error
    }
}
    """

    data = {
        "operationName": None,
        "variables": {
            "id": str(watchfolder.id),
        },
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["startWatchfolder"]["status"] is True
    assert response["data"]["startWatchfolder"]["error"] is None

    watchfolder = application.watchfolders.read(watchfolder.id)
    assert watchfolder.start_at is not None
    assert watchfolder.status == WATCHFOLDER_STATUS_RUNNING

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_stop_watchfolder(application: MainApplication, graphql_schema):
    """
    test_stop_watchfolder

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.read(workflow.id) is not None

    variables = {"source": "/tmp"}
    watchfolder = application.watchfolders.create(
        "/tmp/madam_tests",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
    )

    assert watchfolder.start_at is None
    assert watchfolder.end_at is None
    assert watchfolder.status == WATCHFOLDER_STATUS_STOPPED

    application.watchfolders.start(watchfolder)

    watchfolder = application.watchfolders.read(watchfolder.id)
    assert watchfolder.start_at is not None
    assert watchfolder.end_at is None
    assert watchfolder.status == WATCHFOLDER_STATUS_RUNNING

    mutation_ = """
mutation ($id: ID!) {
    stopWatchfolder(id: $id) {
        status
        error
    }
}
    """

    data = {
        "operationName": None,
        "variables": {
            "id": str(watchfolder.id),
        },
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["stopWatchfolder"]["status"] is True
    assert response["data"]["stopWatchfolder"]["error"] is None

    sleep(1)

    watchfolder = application.watchfolders.read(watchfolder.id)
    assert watchfolder.end_at is not None
    assert watchfolder.status == WATCHFOLDER_STATUS_STOPPED

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_delete_watchfolder(application: MainApplication, graphql_schema):
    """
    test_delete_watchfolder

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.read(workflow.id) is not None

    variables = {"source": "/tmp"}
    watchfolder = application.watchfolders.create(
        "/tmp/madam_tests",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
    )

    mutation_ = """
mutation ($id: ID!) {
    deleteWatchfolder(id: $id) {
        status
        error
    }
}
    """

    data = {
        "operationName": None,
        "variables": {"id": str(watchfolder.id)},
        "query": mutation_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert response["data"]["deleteWatchfolder"]["status"] is True
    assert response["data"]["deleteWatchfolder"]["error"] is None

    with pytest.raises(Exception):
        application.watchfolders.read(watchfolder.id)

    application.workflows.delete(workflow)
