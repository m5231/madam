# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
import datetime
import json
import socket
from multiprocessing import Process
from pathlib import Path
from urllib import request

import pytest
from ariadne.asgi import GQL_CONNECTION_ACK, GQL_CONNECTION_INIT, GQL_DATA, GQL_START
from websockets.client import connect
from websockets.typing import Subprotocol

from madam.domains.application import MainApplication
from madam.domains.entities.events import (
    ApplicationEvent,
    JobEvent,
    TimerEvent,
    WatchfolderEvent,
    WorkflowEvent,
    WorkflowInstanceEvent,
)
from madam.domains.entities.workflow import Workflow
from madam.domains.entities.workflow_instance import STATUS_RUNNING
from madam.slots.graphql import server

# from websockets import connect -> mypy bugs here see: https://github.com/aaugustin/websockets/issues/940

WORKFLOW_PATH = Path(__file__).parents[2].joinpath("assets/ffmpeg/ffmpeg.bpmn")
WORKFLOW_CYCLIC_TIMER_PATH = (
    Path(__file__).parents[2].joinpath("assets/workflows/cyclic_timer.bpmn")
)
WATCHFOLDER_WORKFLOW_PATH = (
    Path(__file__).parents[2].joinpath("assets/workflows/test_create.bpmn")
)

EVENTS_SUBCRIPTION_QUERY = """
subscription {
  events {
    ... on WorkflowEvent {
            type
            workflow {
                id
                version
                name
                content
                sha256
                timer
                created_at
        }
    }
    ... on WorkflowInstanceEvent {
            type
            workflow_instance {
                id
                start_at
                end_at
                status
                input
                output
                error
                workflow {
                    id
                    version
                }
        }
    }
    ... on WatchfolderEvent {
            type
            watchfolder {
                id
                path
                start_at
                end_at
                status
                re_files
                re_dirs
                added_workflow {
                    id
                    version
                }
                added_variables
                modified_workflow {
                    id
                    version
                }
                modified_variables
                deleted_workflow {
                    id
                    version
                }
                deleted_variables
                added_items_key
                modified_items_key
                deleted_items_key
        }
    }
    ... on TimerEvent {
            type
            timer {
                id
                start_at
                end_at
                status
                input
                workflow {
                    id
                    version
                }
                ... on CyclicTimer {
                    repeat
                    interval
                }
                ... on DateTimer {
                    date
                }
        }
    }
    ... on JobEvent {
            type
            job {
                id
                agent_id
                agent_type
                start_at
                end_at
                status
                headers
                input
                output
                error
                workflow_instance {
                    id
                }
            }
    }
    ... on ApplicationEvent {
            type
            application {
                id
                name
                arguments
                start_at
                end_at
                status
                logs
                container_id
                container_name
                container_error
                job  {
                        id
                    }
            }
    }
  }
}
"""


def get_free_port():
    """
    Retur a free tcp port on localhost

    :return:
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(("", 0))
    port = sock.getsockname()[1]
    sock.close()
    return port


@pytest.mark.asyncio
async def test_instance_events(application: MainApplication):
    """
    Test workflow instance events on websocket subscription

    :param application: Application instance
    :return:
    """
    port = get_free_port()
    application.config._config["server"]["port"] = port

    # start graphql server as a subprocess to kill it after the test
    proc = Process(target=server.run, args=(application,), daemon=True)
    proc.start()

    # wait graphql_server to start properly
    await asyncio.sleep(1)

    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    # start subscription connection
    query_ = EVENTS_SUBCRIPTION_QUERY
    # setup uri
    config = application.config.get_root_key("server")
    uri = f"ws://{config['host']}:{config['port']}"
    # connect to server as websocket client
    headers = {"Content-Type": "application/json"}
    async with connect(
        uri, extra_headers=headers, subprotocols=[Subprotocol("graphql-ws")]
    ) as websocket:
        # init graphql-ws connection
        await websocket.send(json.dumps({"type": GQL_CONNECTION_INIT}))
        # start subscription on graphql-ws connection
        await websocket.send(
            json.dumps(
                {
                    "type": GQL_START,
                    "id": "test1",
                    "payload": {"query": query_},
                }
            )
        )
        # check response is ACK
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_CONNECTION_ACK

        # create workflow with an http request on graphql server
        http_uri = f"http://{config['host']}:{config['port']}"
        mutation_query = """
        mutation ($content: String!) {
            createWorkflow(content: $content) {
                status
                error
                workflow {
                    id
                    version
                    sha256
                    }
            }
        }
            """
        variables = {"content": workflow_content}
        data = {"query": mutation_query, "variables": variables}
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        req = request.Request(http_uri, json.dumps(data).encode("utf-8"), headers)
        with request.urlopen(req) as _:
            pass

        # wait workflow instance to start
        await asyncio.sleep(1)

        # wait for subscription event (create workflow)
        response = json.loads(await websocket.recv())
        print(response)
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowEvent.EVENT_TYPE_CREATE
        )
        assert "workflow" in response["payload"]["data"]["events"]
        workflow_dict = response["payload"]["data"]["events"]["workflow"]
        workflow_dict["created_at"] = datetime.datetime.fromisoformat(
            workflow_dict["created_at"]
        )

        # get workflow object from dict
        workflow = Workflow(**workflow_dict)

        # start workflow instance with an http request on graphql server
        http_uri = f"http://{config['host']}:{config['port']}"
        mutation_query = """
        mutation ($id: String!) {
            startWorkflow(id: $id) {
                status
                error
            }
        }
        """
        variables = {"id": workflow.id}
        data = {"query": mutation_query, "variables": variables}
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        req = request.Request(http_uri, json.dumps(data).encode("utf-8"), headers)
        with request.urlopen(req) as _:
            pass

        # wait workflow instance to start
        await asyncio.sleep(1)

        # wait for subscription event (create workflow instance)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowInstanceEvent.EVENT_TYPE_CREATE
        )
        assert "workflow_instance" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["workflow_instance"]
        assert object_["status"] == "running"
        assert object_["end_at"] is None

        while (
            len(
                application.workflow_instances.list(
                    workflow=workflow, status=STATUS_RUNNING
                )
            )
            == 1
        ):
            await asyncio.sleep(0.5)

        # wait for subscription event (create job)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"] == JobEvent.EVENT_TYPE_CREATE
        )
        assert "job" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["job"]
        assert object_["status"] == "running"
        assert object_["end_at"] is None

        # wait for subscription event (create application)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == ApplicationEvent.EVENT_TYPE_CREATE
        )
        assert "application" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["application"]
        assert object_["status"] == "running"
        assert object_["container_id"] is None
        assert object_["end_at"] is None

        # wait for subscription event (update application container_id)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == ApplicationEvent.EVENT_TYPE_UPDATE
        )
        assert "application" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["application"]
        assert object_["status"] == "running"
        assert object_["container_id"] is not None
        assert object_["end_at"] is None

        # wait for subscription event (update application status)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == ApplicationEvent.EVENT_TYPE_UPDATE
        )
        assert "application" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["application"]
        assert object_["status"] == "complete"
        assert object_["container_id"] is not None
        assert object_["end_at"] is not None

        # wait for subscription event (update job status)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"] == JobEvent.EVENT_TYPE_UPDATE
        )
        assert "job" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["job"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

        # wait for subscription event (update workflow instance status)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowInstanceEvent.EVENT_TYPE_UPDATE
        )
        assert "workflow_instance" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["workflow_instance"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

        # delete workflow instance with an http request on graphql server
        http_uri = f"http://{config['host']}:{config['port']}"
        mutation_query = """
        mutation ($id: String!) {
            deleteWorkflow(id: $id) {
                status
                error
            }
        }
        """
        variables = {"id": workflow.id}
        data = {"query": mutation_query, "variables": variables}
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        req = request.Request(http_uri, json.dumps(data).encode("utf-8"), headers)
        with request.urlopen(req) as _:
            pass

        # wait workflow instance to start
        await asyncio.sleep(1)

        # wait for subscription event (delete application)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == ApplicationEvent.EVENT_TYPE_DELETE
        )
        assert "application" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["application"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

        # wait for subscription event (job delete)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"] == JobEvent.EVENT_TYPE_DELETE
        )
        assert "job" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["job"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

        # wait for subscription event (workflow instance delete)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowInstanceEvent.EVENT_TYPE_DELETE
        )
        assert "workflow_instance" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["workflow_instance"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

        # wait for subscription event (delete workflow)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowEvent.EVENT_TYPE_DELETE
        )
        assert "workflow" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["workflow"]
        assert object_["id"] == workflow.id
        assert object_["version"] == workflow.version

    # shutdown graphql server
    proc.terminate()

    application.workflows.delete(workflow)


@pytest.mark.asyncio
async def test_timer_events(application: MainApplication):
    """
    Test timer events on websocket subscription

    :param application: Application instance
    :return:
    """
    port = get_free_port()
    application.config._config["server"]["port"] = port

    # start graphql server as a subprocess to kill it after the test
    proc = Process(target=server.run, args=(application,), daemon=True)
    proc.start()

    # wait graphql_server to start properly
    await asyncio.sleep(1)

    with open(WORKFLOW_CYCLIC_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    # start subscription connection
    query_ = EVENTS_SUBCRIPTION_QUERY
    # setup uri
    config = application.config.get_root_key("server")
    uri = f"ws://{config['host']}:{config['port']}"
    # connect to server as websocket client
    headers = {"Content-Type": "application/json"}
    async with connect(
        uri, extra_headers=headers, subprotocols=[Subprotocol("graphql-ws")]
    ) as websocket:
        # init graphql-ws connection
        await websocket.send(json.dumps({"type": GQL_CONNECTION_INIT}))
        # start subscription on graphql-ws connection
        await websocket.send(
            json.dumps(
                {
                    "type": GQL_START,
                    "id": "test1",
                    "payload": {"query": query_},
                }
            )
        )
        # check response is ACK
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_CONNECTION_ACK

        # start workflow instance with an http request on graphql server
        http_uri = f"http://{config['host']}:{config['port']}"
        mutation_query = """
        mutation ($id: String!, $variables: JSON) {
            startWorkflow(id: $id, variables: $variables) {
                status
                error
            }
        }
        """
        variables = {"id": workflow.id, "variables": json.dumps({"source": "/tmp"})}
        data = {"query": mutation_query, "variables": variables}
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        req = request.Request(http_uri, json.dumps(data).encode("utf-8"), headers)
        with request.urlopen(req) as _:
            pass

        # wait workflow instance to start
        await asyncio.sleep(1)

        # wait for subscription event (create timer)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == TimerEvent.EVENT_TYPE_CREATE
        )
        assert "timer" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["timer"]
        assert object_["status"] == "running"
        assert object_["end_at"] is None

        # wait for subscription event (create workflow instance)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowInstanceEvent.EVENT_TYPE_CREATE
        )
        assert "workflow_instance" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["workflow_instance"]
        assert object_["status"] == "running"
        assert object_["end_at"] is None

        while (
            len(
                application.workflow_instances.list(
                    workflow=workflow, status=STATUS_RUNNING
                )
            )
            == 1
        ):
            await asyncio.sleep(0.5)

        # wait for subscription event (create job)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"] == JobEvent.EVENT_TYPE_CREATE
        )
        assert "job" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["job"]
        assert object_["status"] == "running"
        assert object_["end_at"] is None

        # wait for subscription event (update job status)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"] == JobEvent.EVENT_TYPE_UPDATE
        )
        assert "job" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["job"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

        # wait for subscription event (update workflow instance status)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowInstanceEvent.EVENT_TYPE_UPDATE
        )
        assert "workflow_instance" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["workflow_instance"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

        # wait for subscription event (create workflow instance)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowInstanceEvent.EVENT_TYPE_CREATE
        )
        assert "workflow_instance" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["workflow_instance"]
        assert object_["status"] == "running"
        assert object_["end_at"] is None

        while (
            len(
                application.workflow_instances.list(
                    workflow=workflow, status=STATUS_RUNNING
                )
            )
            == 1
        ):
            await asyncio.sleep(0.5)

        # wait for subscription event (create job)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"] == JobEvent.EVENT_TYPE_CREATE
        )
        assert "job" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["job"]
        assert object_["status"] == "running"
        assert object_["end_at"] is None

        # wait for subscription event (update job status)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"] == JobEvent.EVENT_TYPE_UPDATE
        )
        assert "job" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["job"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

        # wait for subscription event (update workflow instance status)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowInstanceEvent.EVENT_TYPE_UPDATE
        )
        assert "workflow_instance" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["workflow_instance"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

        # wait for subscription event (update timer)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == TimerEvent.EVENT_TYPE_UPDATE
        )
        assert "timer" in response["payload"]["data"]["events"]
        object_ = response["payload"]["data"]["events"]["timer"]
        assert object_["status"] == "complete"
        assert object_["end_at"] is not None

    # shutdown graphql server
    proc.terminate()

    application.workflows.delete(workflow)


@pytest.mark.asyncio
async def test_watchfolder_events(application: MainApplication):
    """
    Test watchfolder events on websocket subscription

    :param application: Application instance
    :return:
    """
    port = get_free_port()
    application.config._config["server"]["port"] = port

    # start graphql server as a subprocess to kill it after the test
    proc = Process(target=server.run, args=(application,), daemon=True)
    proc.start()

    # wait graphql_server to start properly
    await asyncio.sleep(1)

    with open(WATCHFOLDER_WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    # start subscription connection
    query_ = EVENTS_SUBCRIPTION_QUERY

    # setup uri
    config = application.config.get_root_key("server")
    uri = f"ws://{config['host']}:{config['port']}"
    # connect to server as websocket client
    headers = {"Content-Type": "application/json"}
    async with connect(
        uri, extra_headers=headers, subprotocols=[Subprotocol("graphql-ws")]
    ) as websocket:
        # init graphql-ws connection
        await websocket.send(json.dumps({"type": GQL_CONNECTION_INIT}))
        # start subscription on graphql-ws connection
        await websocket.send(
            json.dumps(
                {
                    "type": GQL_START,
                    "id": "test1",
                    "payload": {"query": query_},
                }
            )
        )
        # check response is ACK
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_CONNECTION_ACK

        # create workflow with an http request on graphql server
        http_uri = f"http://{config['host']}:{config['port']}"
        mutation_query = """
        mutation ($content: String!) {
            createWorkflow(content: $content) {
                status
                error
                workflow {
                    id
                    version
                    sha256
                    }
            }
        }
            """
        variables = {"content": workflow_content}
        data = {"query": mutation_query, "variables": variables}
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        req = request.Request(http_uri, json.dumps(data).encode("utf-8"), headers)
        with request.urlopen(req) as _:
            pass

        # wait workflow instance to start
        await asyncio.sleep(1)

        # wait for subscription event (create workflow)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WorkflowEvent.EVENT_TYPE_CREATE
        )
        assert "workflow" in response["payload"]["data"]["events"]
        workflow_dict = response["payload"]["data"]["events"]["workflow"]
        workflow_dict["created_at"] = datetime.datetime.fromisoformat(
            workflow_dict["created_at"]
        )

        # get workflow object from dict
        workflow = Workflow(**workflow_dict)

        # create watchfolder with an http request on graphql server
        http_uri = f"http://{config['host']}:{config['port']}"
        folder_path = "/tmp/madam_tests"
        re_files = r".+\.mp4"
        re_dirs = r"sources_.+"

        mutation_query = """
        mutation (
            $path: String!,
            $re_files: String!,
            $re_dirs: String!,
            $added_workflow: WorkflowInput!,
            $added_variables: JSON!
        ) {
            createWatchfolder(
                path: $path,
                re_files: $re_files,
                re_dirs: $re_dirs,
                added_workflow: $added_workflow,
                added_variables: $added_variables
            ) {
                status
                error
                watchfolder {
                    id
                    path
                    re_files
                    re_dirs
                    added_workflow {
                            id
                        }
                    added_variables
                    modified_workflow {
                            id
                        }
                    modified_variables
                    deleted_workflow {
                            id
                        }
                    deleted_variables
                    added_items_key
                    modified_items_key
                    deleted_items_key
                    }
            }
        }
        """

        variables = {"source": "/tmp/madam_tests"}
        data = {
            "query": mutation_query,
            "variables": {
                "path": folder_path,
                "re_files": re_files,
                "re_dirs": re_dirs,
                "added_workflow": {"id": workflow.id},
                "added_variables": variables,
            },
        }
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        req = request.Request(http_uri, json.dumps(data).encode("utf-8"), headers)
        with request.urlopen(req) as request_handler:
            response = json.loads(request_handler.read())
        try:
            assert response["data"]["createWatchfolder"]["status"] is True
        except AssertionError as exception:
            raise Exception(
                response["data"]["createWatchfolder"]["error"]
            ) from exception

        # wait for subscription event (create watchfolder)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WatchfolderEvent.EVENT_TYPE_CREATE
        )
        assert "watchfolder" in response["payload"]["data"]["events"]
        watchfolder_dict = response["payload"]["data"]["events"]["watchfolder"]
        watchfolder_id = watchfolder_dict["id"]

        # update watchfolder with an http request on graphql server
        http_uri = f"http://{config['host']}:{config['port']}"
        mutation_query = """
        mutation ($id: ID!, $path: String!, $re_files: String, $re_dirs: String!) {
            updateWatchfolder(
            id: $id,
            path: $path,
            re_files: $re_files,
            re_dirs: $re_dirs
            ) {
                status
                error
                watchfolder {
                    id
                    path
                    re_files
                    re_dirs
                }
            }
        }
        """
        updated_path = "/tmp"
        updated_re_files = r".*\.mp4"
        updated_re_dirs = r"sources_.*"
        variables = {
            "id": str(watchfolder_id),
            "path": updated_path,
            "re_files": updated_re_files,
            "re_dirs": updated_re_dirs,
        }
        data = {"query": mutation_query, "variables": variables}
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        req = request.Request(http_uri, json.dumps(data).encode("utf-8"), headers)
        with request.urlopen(req) as _:
            pass

        # wait for subscription event (update watchfolder)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WatchfolderEvent.EVENT_TYPE_UPDATE
        )
        assert "watchfolder" in response["payload"]["data"]["events"]

        # delete watchfolder with an http request on graphql server
        http_uri = f"http://{config['host']}:{config['port']}"
        mutation_query = """
        mutation ($id: ID!) {
            deleteWatchfolder(id: $id) {
                status
                error
            }
        }
        """
        variables = {"id": watchfolder_id}
        data = {"query": mutation_query, "variables": variables}
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        req = request.Request(http_uri, json.dumps(data).encode("utf-8"), headers)
        with request.urlopen(req) as _:
            pass

        # wait for subscription event (delete watchfolder)
        response = json.loads(await websocket.recv())
        assert response["type"] == GQL_DATA
        assert "payload" in response
        assert "data" in response["payload"]
        assert "events" in response["payload"]["data"]
        assert "type" in response["payload"]["data"]["events"]
        assert (
            response["payload"]["data"]["events"]["type"]
            == WatchfolderEvent.EVENT_TYPE_DELETE
        )
        assert "watchfolder" in response["payload"]["data"]["events"]

    # shutdown graphql server
    proc.terminate()

    application.workflows.delete(workflow)
