# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime, timedelta
from pathlib import Path
from typing import Any, Dict

from ariadne import graphql_sync

from madam import __version__
from madam.domains.application import MainApplication
from madam.domains.entities.application import (
    STATUS_RUNNING as APPLICATION_STATUS_RUNNING,
)
from madam.domains.entities.job import STATUS_RUNNING as JOB_STATUS_RUNNING
from madam.domains.entities.timer import STATUS_RUNNING as TIMER_STATUS_RUNNING
from madam.domains.entities.watchfolder import (
    STATUS_STOPPED as WATCHFOLDER_STATUS_STOPPED,
)
from madam.domains.entities.workflow_instance import (
    STATUS_RUNNING as INSTANCE_STATUS_RUNNING,
)

WORKFLOW_PATH = Path(__file__).parents[2].joinpath("assets/workflows/test_create.bpmn")
WORKFLOW_DATE_TIMER_PATH = (
    Path(__file__).parents[2].joinpath("assets/workflows/date_timer.bpmn")
)


def test_version(application: MainApplication, graphql_schema):
    """
    test_version

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    query_ = """
    query {
        version
    }
    """

    data: Dict[str, Any] = {"operationName": None, "variables": {}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert response["data"]["version"] == __version__


def test_agents(application: MainApplication, graphql_schema):
    """
    test_agents

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    query_ = """
    query {
        agents {
            name
            help
            applications {
                label
                name
                version
            }
        }
    }
    """

    data: Dict[str, Any] = {"operationName": None, "variables": {}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert len(response["data"]["agents"]) == 9

    assert "name" in response["data"]["agents"][0]
    assert response["data"]["agents"][0]["name"] == "bash"
    assert "help" in response["data"]["agents"][0]
    assert response["data"]["agents"][0]["help"] is not None
    assert response["data"]["agents"][0]["help"] != ""
    assert "applications" in response["data"]["agents"][0]
    assert isinstance(response["data"]["agents"][0]["applications"], list)
    assert len(response["data"]["agents"][0]["applications"]) == 1
    assert "label" in response["data"]["agents"][0]["applications"][0]
    assert response["data"]["agents"][0]["applications"][0]["label"] == "bash"
    assert "name" in response["data"]["agents"][0]["applications"][0]
    assert response["data"]["agents"][0]["applications"][0]["name"] == "bash"
    assert "version" in response["data"]["agents"][0]["applications"][0]
    assert response["data"]["agents"][0]["applications"][0]["version"] == "latest"

    assert "name" in response["data"]["agents"][8]
    assert response["data"]["agents"][8]["name"] == "scenes"
    assert "help" in response["data"]["agents"][8]
    assert response["data"]["agents"][8]["help"] is not None
    assert response["data"]["agents"][8]["help"] != ""
    assert "applications" in response["data"]["agents"][8]
    assert isinstance(response["data"]["agents"][8]["applications"], list)
    assert len(response["data"]["agents"][8]["applications"]) == 1
    assert "label" in response["data"]["agents"][8]["applications"][0]
    assert response["data"]["agents"][8]["applications"][0]["label"] == "ffmpeg"
    assert "name" in response["data"]["agents"][8]["applications"][0]
    assert response["data"]["agents"][8]["applications"][0]["name"] == "ffmpeg"
    assert "version" in response["data"]["agents"][8]["applications"][0]
    assert response["data"]["agents"][8]["applications"][0]["version"] == "latest"


def test_agents_only_names(application: MainApplication, graphql_schema):
    """
    test_agents_only_names

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    query_ = """
    query {
        agents {
            name
        }
    }
    """

    data: Dict[str, Any] = {"operationName": None, "variables": {}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert len(response["data"]["agents"]) == 9
    assert "name" in response["data"]["agents"][0]
    assert response["data"]["agents"][0]["name"] == "bash"
    assert "name" in response["data"]["agents"][8]
    assert response["data"]["agents"][8]["name"] == "scenes"


def test_agent(application: MainApplication, graphql_schema):
    """
    test_agent

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    query_ = """
    query ($name: String!) {
        agent(name: $name) {
            name
            help
            applications {
                label
                name
                version
            }
        }
    }
    """

    data: Dict[str, Any] = {
        "operationName": None,
        "variables": {"name": "ffmpeg"},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)

    assert success is True
    assert response["data"]["agent"] is not None
    assert "name" in response["data"]["agent"]
    assert response["data"]["agent"]["name"] == "ffmpeg"
    assert "help" in response["data"]["agent"]
    assert response["data"]["agent"]["help"] is not None
    assert response["data"]["agent"]["help"] != ""
    assert "applications" in response["data"]["agent"]
    assert isinstance(response["data"]["agent"]["applications"], list)
    assert len(response["data"]["agent"]["applications"]) == 1
    assert "label" in response["data"]["agent"]["applications"][0]
    assert response["data"]["agent"]["applications"][0]["label"] == "ffmpeg"
    assert "name" in response["data"]["agent"]["applications"][0]
    assert response["data"]["agent"]["applications"][0]["name"] == "ffmpeg"
    assert "version" in response["data"]["agent"]["applications"][0]
    assert response["data"]["agent"]["applications"][0]["version"] == "latest"


def test_workflow(application: MainApplication, graphql_schema):
    """
    test_workflow

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content_v1 = fh.read()

    # create three versions of the same workflow id
    workflow_content_v2 = workflow_content_v1.replace(
        "Test Create Name 01", "Test Create Name 02"
    )

    workflow_v1 = application.workflows.create(workflow_content_v1)
    workflow_v2 = application.workflows.create(workflow_content_v2)

    query_ = """
    query ($id: String!, $version: Int) {
        workflow (id: $id, version: $version) {
            id
            version
            sha256
            created_at
        }
    }
    """

    data: Dict[str, Any] = {
        "operationName": None,
        "variables": {"id": workflow_v1.id, "version": workflow_v1.version},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflow"]["id"] == workflow_v1.id
    assert response["data"]["workflow"]["version"] == 1
    assert (
        response["data"]["workflow"]["sha256"]
        == "5d0838d15fa8c7035bd352ad7dff9e46c95edd6dda9a18f522976ce28b911aa1"
    )
    assert (
        isinstance(
            datetime.fromisoformat(response["data"]["workflow"]["created_at"]), datetime
        )
        is True
    )

    query_ = """
    query ($id: String!) {
        workflow (id: $id) {
            id
            version
            sha256
        }
    }
    """
    data = {
        "operationName": None,
        "variables": {"id": workflow_v2.id},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflow"] == {
        "id": "test_create_id",
        "sha256": "e335a80f2a925f9b430f216678a68efbccc362e819059b0067bee38d385ef8d2",
        "version": 2,
    }

    application.workflows.delete(workflow_v1)


def test_workflows(application: MainApplication, graphql_schema):
    """
    test_workflows

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    # empty list
    query_ = """
    query {
        workflows {
            count
            result {
                id
                version
                sha256
            }
        }
    }
    """

    data: Dict[str, Any] = {"operationName": None, "variables": {}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflows"]["count"] == 0
    assert len(response["data"]["workflows"]["result"]) == 0

    # 10 workflows
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    # create nine other versions of the same workflow id
    for index in range(2, 11):
        workflow_content_tmp = workflow_content.replace(
            "Test Create Name 01", f"Test Create Name {index:02d}"
        )
        application.workflows.create(workflow_content_tmp)

    query_ = """
    query {
        workflows {
            count
            result {
                id
                version
                sha256
            }
        }
    }
    """

    data = {"operationName": None, "variables": {}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflows"]["count"] == 10
    assert response["data"]["workflows"]["result"][0] == {
        "id": "test_create_id",
        "sha256": "5d0838d15fa8c7035bd352ad7dff9e46c95edd6dda9a18f522976ce28b911aa1",
        "version": 1,
    }
    assert response["data"]["workflows"]["result"][9] == {
        "id": "test_create_id",
        "sha256": "6d7d4125633a5aeb98c8a1be7dff0b8d24807b390619b78dcb14eed3fe5784fc",
        "version": 10,
    }
    # Paginate forward with 5 elements per page
    query_ = """
    query ($offset: Int!, $limit: Int!) {
        workflows (offset: $offset, limit: $limit) {
            count
            result {
                id
                version
                sha256
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"offset": 5, "limit": 5},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflows"]["count"] == 10
    assert response["data"]["workflows"]["result"][0] == {
        "id": "test_create_id",
        "sha256": "2c86f97d2aa9b613c0bbf12128427e1242f388049d7eaad6ce786f9959bcdd7a",
        "version": 6,
    }
    assert response["data"]["workflows"]["result"][4] == {
        "id": "test_create_id",
        "sha256": "6d7d4125633a5aeb98c8a1be7dff0b8d24807b390619b78dcb14eed3fe5784fc",
        "version": 10,
    }

    # Descending column
    query_ = """
    query ($sort_column: WorkflowSortColumn!, $sort_ascending: Boolean!) {
        workflows (sort_column: $sort_column, sort_ascending: $sort_ascending) {
            count
            result {
                id
                version
                sha256
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"sort_column": "version", "sort_ascending": False},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflows"]["count"] == 10
    assert response["data"]["workflows"]["result"][0] == {
        "id": "test_create_id",
        "sha256": "6d7d4125633a5aeb98c8a1be7dff0b8d24807b390619b78dcb14eed3fe5784fc",
        "version": 10,
    }
    assert response["data"]["workflows"]["result"][9] == {
        "id": "test_create_id",
        "sha256": "5d0838d15fa8c7035bd352ad7dff9e46c95edd6dda9a18f522976ce28b911aa1",
        "version": 1,
    }


def test_workflow_instance(application: MainApplication, graphql_schema):
    """
    test_instance

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)

    query_ = """
    query ($id: ID!) {
        workflow_instance (id: $id) {
            id
            start_at
            end_at
            status
            input
            output
            error
            workflow {
                id
                version
                sha256
                created_at
            }
        }
    }
    """
    data = {
        "operationName": None,
        "variables": {"id": str(workflow_instance.id)},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflow_instance"]["status"] == INSTANCE_STATUS_RUNNING
    assert "id" in response["data"]["workflow_instance"]
    assert "start_at" in response["data"]["workflow_instance"]
    assert "end_at" in response["data"]["workflow_instance"]
    assert "input" in response["data"]["workflow_instance"]
    assert "output" in response["data"]["workflow_instance"]
    assert "error" in response["data"]["workflow_instance"]
    assert "workflow" in response["data"]["workflow_instance"]
    assert response["data"]["workflow_instance"]["workflow"]["id"] == workflow.id
    assert "version" in response["data"]["workflow_instance"]["workflow"]
    assert "created_at" in response["data"]["workflow_instance"]["workflow"]
    assert "sha256" in response["data"]["workflow_instance"]["workflow"]

    application.workflows.delete(workflow)


def test_instances(application: MainApplication, graphql_schema):
    """
    test_instances

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    workflow = application.workflows.create(workflow_content)

    # empty list
    query_ = """
    query ($workflow: WorkflowInput) {
        workflow_instances (workflow: $workflow) {
            count
            result {
                id
                status
            }
        }
    }
    """

    data: Dict[str, Any] = {
        "operationName": None,
        "variables": {"workflow": {"id": workflow.id}},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflow_instances"]["count"] == 0
    assert len(response["data"]["workflow_instances"]["result"]) == 0

    # 10 instances
    for _ in range(0, 10):
        application.workflow_instances.create(workflow)

    query_ = """
    query {
        workflow_instances {
            count
            result {
                id
                status
                start_at
            }
        }
    }
    """

    data = {"operationName": None, "variables": {}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflow_instances"]["count"] == 10
    assert len(response["data"]["workflow_instances"]["result"]) == 10
    assert "id" in response["data"]["workflow_instances"]["result"][0]
    assert "status" in response["data"]["workflow_instances"]["result"][0]
    assert "start_at" in response["data"]["workflow_instances"]["result"][0]
    assert (
        response["data"]["workflow_instances"]["result"][0]["start_at"]
        < response["data"]["workflow_instances"]["result"][9]["start_at"]
    )

    # Paginate forward with 5 elements per page
    query_ = """
    query ($offset: Int!, $limit: Int!) {
        workflow_instances (offset: $offset, limit: $limit) {
            count
            result {
                id
                start_at
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"offset": 5, "limit": 5},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflow_instances"]["count"] == 10
    assert len(response["data"]["workflow_instances"]["result"]) == 5

    # Descending column
    query_ = """
    query ($sort_column: WorkflowInstanceSortColumn!, $sort_ascending: Boolean!) {
        workflow_instances (sort_column: $sort_column, sort_ascending: $sort_ascending) {
            count
            result {
                id
                start_at
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"sort_column": "start_at", "sort_ascending": False},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["workflow_instances"]["count"] == 10
    assert len(response["data"]["workflow_instances"]["result"]) == 10
    assert (
        response["data"]["workflow_instances"]["result"][0]["start_at"]
        > response["data"]["workflow_instances"]["result"][9]["start_at"]
    )

    application.workflows.delete(workflow)


def test_timer(application: MainApplication, graphql_schema):
    """
    test_timer

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_DATE_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    timer = application.timers.create_date_timer(workflow, datetime.now())

    query_ = """
    query ($id: ID!) {
        timer (id: $id) {
            id
            status
            start_at
            end_at
            input
            ... on DateTimer {
                    date
                }
            workflow {
                id
                version
            }
        }
    }
    """
    data = {"operationName": None, "variables": {"id": str(timer.id)}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["timer"]["status"] == TIMER_STATUS_RUNNING
    assert "id" in response["data"]["timer"]
    assert "start_at" in response["data"]["timer"]
    assert "end_at" in response["data"]["timer"]
    assert "input" in response["data"]["timer"]
    assert "date" in response["data"]["timer"]
    assert "workflow" in response["data"]["timer"]
    assert response["data"]["timer"]["workflow"]["id"] == workflow.id
    assert "version" in response["data"]["timer"]["workflow"]

    application.workflows.delete(workflow)


def test_timers(application: MainApplication, graphql_schema):
    """
    test_timers

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instancema:
    :return:
    """
    with open(WORKFLOW_DATE_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    workflow = application.workflows.create(workflow_content)

    # empty list
    query_ = """
    query ($workflow: WorkflowInput) {
        timers (workflow: $workflow) {
            count
            result {
                id
                status
            }
        }
    }
    """

    data: Dict[str, Any] = {
        "operationName": None,
        "variables": {"workflow": {"id": workflow.id}},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["timers"]["count"] == 0
    assert len(response["data"]["timers"]["result"]) == 0

    # 10 instances
    for _ in range(0, 10):
        application.timers.create_cyclic_timer(workflow, 5, timedelta(1))

    query_ = """
    query {
        timers {
            count
            result {
                id
                status
                start_at
                ... on CyclicTimer {
                        interval
                }
            }
        }
    }
    """

    data = {"operationName": None, "variables": {}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["timers"]["count"] == 10
    assert len(response["data"]["timers"]["result"]) == 10
    assert "id" in response["data"]["timers"]["result"][0]
    assert "status" in response["data"]["timers"]["result"][0]
    assert "start_at" in response["data"]["timers"]["result"][0]
    assert "interval" in response["data"]["timers"]["result"][0]
    assert response["data"]["timers"]["result"][0]["interval"] == "P1DT00H00MS"
    assert (
        response["data"]["timers"]["result"][0]["start_at"]
        < response["data"]["timers"]["result"][9]["start_at"]
    )

    # Paginate forward with 5 elements per page
    query_ = """
    query ($offset: Int!, $limit: Int!) {
        timers (offset: $offset, limit: $limit) {
            count
            result {
                id
                start_at
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"offset": 5, "limit": 5},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["timers"]["count"] == 10
    assert len(response["data"]["timers"]["result"]) == 5

    # Descending column
    query_ = """
    query ($sort_column: TimerSortColumn!, $sort_ascending: Boolean!) {
        timers (sort_column: $sort_column, sort_ascending: $sort_ascending) {
            count
            result {
                id
                start_at
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"sort_column": "start_at", "sort_ascending": False},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["timers"]["count"] == 10
    assert len(response["data"]["timers"]["result"]) == 10
    assert (
        response["data"]["timers"]["result"][0]["start_at"]
        > response["data"]["timers"]["result"][9]["start_at"]
    )

    application.workflows.delete(workflow)


def test_job(application: MainApplication, graphql_schema):
    """
    test_job

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)

    job = application.jobs.create(workflow_instance, "ffmpeg_agent", "ffmpeg", {}, {})

    query_ = """
    query ($id: ID!) {
        job (id: $id) {
            id
            status
            start_at
            end_at
            input
            output
            error
            headers
            agent_type
            agent_id
            workflow_instance {
                id
            }
        }
    }
    """
    data = {"operationName": None, "variables": {"id": str(job.id)}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["job"]["status"] == JOB_STATUS_RUNNING
    assert "id" in response["data"]["job"]
    assert "start_at" in response["data"]["job"]
    assert "end_at" in response["data"]["job"]
    assert "input" in response["data"]["job"]
    assert "output" in response["data"]["job"]
    assert "headers" in response["data"]["job"]
    assert "error" in response["data"]["job"]
    assert "agent_id" in response["data"]["job"]
    assert "agent_type" in response["data"]["job"]
    assert "workflow_instance" in response["data"]["job"]
    assert response["data"]["job"]["workflow_instance"]["id"] == str(
        workflow_instance.id
    )

    application.workflows.delete(workflow)


def test_jobs(application: MainApplication, graphql_schema):
    """
    test_jobs

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)

    # empty list
    query_ = """
    query ($workflow_instance_id: ID) {
        jobs (workflow_instance_id: $workflow_instance_id) {
            count
            result {
                id
                status
            }
        }
    }
    """

    data: Dict[str, Any] = {
        "operationName": None,
        "variables": {"workflow_instance_id": str(workflow_instance.id)},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["jobs"]["count"] == 0
    assert len(response["data"]["jobs"]["result"]) == 0

    # 10 jobs
    for index in range(0, 10):
        application.jobs.create(
            workflow_instance,
            f"agent_ffmpeg_{index}",
            "ffmpeg",
            {"arguments": "-y -i {{source}}"},
            {"source": "/path/filename"},
        )

    query_ = """
    query {
        jobs {
            count
            result {
                id
                agent_id
                status
                start_at
                input
            }
        }
    }
    """

    data = {"operationName": None, "variables": {}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["jobs"]["count"] == 10
    assert len(response["data"]["jobs"]["result"]) == 10
    assert "id" in response["data"]["jobs"]["result"][0]
    assert "status" in response["data"]["jobs"]["result"][0]
    assert "agent_id" in response["data"]["jobs"]["result"][0]
    assert "start_at" in response["data"]["jobs"]["result"][0]
    assert response["data"]["jobs"]["result"][0]["agent_id"] == "agent_ffmpeg_0"
    assert response["data"]["jobs"]["result"][9]["agent_id"] == "agent_ffmpeg_9"
    assert (
        response["data"]["jobs"]["result"][0]["start_at"]
        < response["data"]["jobs"]["result"][9]["start_at"]
    )

    # Paginate forward with 5 elements per page
    query_ = """
    query ($offset: Int!, $limit: Int!) {
        jobs (offset: $offset, limit: $limit) {
            count
            result {
                id
                agent_id
                start_at
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"offset": 5, "limit": 5},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["jobs"]["count"] == 10
    assert len(response["data"]["jobs"]["result"]) == 5
    assert response["data"]["jobs"]["result"][0]["agent_id"] == "agent_ffmpeg_5"
    assert response["data"]["jobs"]["result"][4]["agent_id"] == "agent_ffmpeg_9"

    # Descending column
    query_ = """
    query ($sort_column: JobSortColumn!, $sort_ascending: Boolean!) {
        jobs (sort_column: $sort_column, sort_ascending: $sort_ascending) {
            count
            result {
                id
                agent_id
                start_at
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"sort_column": "start_at", "sort_ascending": False},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["jobs"]["count"] == 10
    assert len(response["data"]["jobs"]["result"]) == 10
    assert response["data"]["jobs"]["result"][9]["agent_id"] == "agent_ffmpeg_0"
    assert response["data"]["jobs"]["result"][0]["agent_id"] == "agent_ffmpeg_9"
    assert (
        response["data"]["jobs"]["result"][0]["start_at"]
        > response["data"]["jobs"]["result"][9]["start_at"]
    )

    application.workflows.delete(workflow)


def test_application(application: MainApplication, graphql_schema):
    """
    test_application

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)

    job = application.jobs.create(workflow_instance, "ffmpeg_agent", "ffmpeg", {}, {})
    application_ = application.applications.create(
        job, "app_name", "app_version", "arguments"
    )
    application.applications.update(application_, container_id="container_id_xxxx")

    query_ = """
    query ($id: ID!) {
        application (id: $id) {
            id
            status
            start_at
            end_at
            name
            arguments
            logs
            container_id
            container_name
            container_error
            job {
                id
            }
        }
    }
    """
    data = {
        "operationName": None,
        "variables": {"id": str(application_.id)},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["application"]["status"] == APPLICATION_STATUS_RUNNING
    assert "id" in response["data"]["application"]
    assert "start_at" in response["data"]["application"]
    assert "end_at" in response["data"]["application"]
    assert "name" in response["data"]["application"]
    assert "arguments" in response["data"]["application"]
    assert "logs" in response["data"]["application"]
    assert "container_id" in response["data"]["application"]
    assert "container_name" in response["data"]["application"]
    assert "container_error" in response["data"]["application"]
    assert response["data"]["application"]["job"]["id"] == str(job.id)

    application.workflows.delete(workflow)


def test_applications(application: MainApplication, graphql_schema):
    """
    test_applications

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)

    job = application.jobs.create(
        workflow_instance,
        "agent_ffmpeg",
        "ffmpeg",
        {"arguments": "-y -i {{source}}"},
        {"source": "/path/filename"},
    )

    # empty list
    query_ = """
    query ($job_id: ID) {
        applications (job_id: $job_id) {
            count
            result {
                id
                name
                status
            }
        }
    }
    """

    data: Dict[str, Any] = {
        "operationName": None,
        "variables": {"job_id": str(job.id)},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["applications"]["count"] == 0
    assert len(response["data"]["applications"]["result"]) == 0

    # 10 applications
    for index in range(0, 10):
        application.applications.create(
            job, f"application_ffmpeg_{index}", "latest", "-y -i {{source}}"
        )

    query_ = """
    query {
        applications {
            count
            result {
                id
                name
                status
                start_at
            }
        }
    }
    """

    data = {"operationName": None, "variables": {}, "query": query_}
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["applications"]["count"] == 10
    assert len(response["data"]["applications"]["result"]) == 10
    assert "id" in response["data"]["applications"]["result"][0]
    assert "status" in response["data"]["applications"]["result"][0]
    assert "name" in response["data"]["applications"]["result"][0]
    assert "start_at" in response["data"]["applications"]["result"][0]
    assert (
        response["data"]["applications"]["result"][0]["name"] == "application_ffmpeg_0"
    )
    assert (
        response["data"]["applications"]["result"][9]["name"] == "application_ffmpeg_9"
    )
    assert (
        response["data"]["applications"]["result"][0]["start_at"]
        < response["data"]["applications"]["result"][9]["start_at"]
    )

    # Paginate forward with 5 elements per page
    query_ = """
    query ($offset: Int!, $limit: Int!) {
        applications (offset: $offset, limit: $limit) {
            count
            result {
                id
                name
                start_at
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"offset": 5, "limit": 5},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["applications"]["count"] == 10
    assert len(response["data"]["applications"]["result"]) == 5
    assert (
        response["data"]["applications"]["result"][0]["name"] == "application_ffmpeg_5"
    )
    assert (
        response["data"]["applications"]["result"][4]["name"] == "application_ffmpeg_9"
    )

    # Descending column
    query_ = """
    query ($sort_column: JobSortColumn!, $sort_ascending: Boolean!) {
        applications (sort_column: $sort_column, sort_ascending: $sort_ascending) {
            count
            result {
                id
                name
                start_at
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"sort_column": "start_at", "sort_ascending": False},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["applications"]["count"] == 10
    assert len(response["data"]["applications"]["result"]) == 10
    assert (
        response["data"]["applications"]["result"][9]["name"] == "application_ffmpeg_0"
    )
    assert (
        response["data"]["applications"]["result"][0]["name"] == "application_ffmpeg_9"
    )
    assert (
        response["data"]["applications"]["result"][0]["start_at"]
        > response["data"]["applications"]["result"][9]["start_at"]
    )

    application.workflows.delete(workflow)


def test_watchfolder(application: MainApplication, graphql_schema):
    """
    test_watchfolder

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    variables = {"source": "/tmp"}
    re_files = r".+\.mp4"
    re_dirs = r"source_.+"
    watchfolder = application.watchfolders.create(
        "/tmp/madam_tests",
        re_files=re_files,
        re_dirs=re_dirs,
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items_key",
        modified_items_key="modified_items_key",
        deleted_items_key="deleted_items_key",
    )

    query_ = """
    query ($id: ID!) {
        watchfolder (id: $id) {
            id
            path
            start_at
            end_at
            status
            re_files
            re_dirs
            added_workflow {
                id
                version
                sha256
                created_at
            }
            added_variables
            modified_workflow {
                id
                version
                sha256
                created_at
            }
            modified_variables
            deleted_workflow {
                id
                version
                sha256
                created_at
            }
            deleted_variables
            added_items_key
            modified_items_key
            deleted_items_key
        }
    }
    """
    data = {
        "operationName": None,
        "variables": {"id": str(watchfolder.id)},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["watchfolder"]["status"] == WATCHFOLDER_STATUS_STOPPED
    assert "id" in response["data"]["watchfolder"]
    assert "path" in response["data"]["watchfolder"]
    assert "start_at" in response["data"]["watchfolder"]
    assert "end_at" in response["data"]["watchfolder"]
    assert "re_files" in response["data"]["watchfolder"]
    assert "re_dirs" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["re_files"] == re_files
    assert response["data"]["watchfolder"]["re_dirs"] == re_dirs
    assert "added_workflow" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["added_workflow"]["id"] == workflow.id
    assert "version" in response["data"]["watchfolder"]["added_workflow"]
    assert "created_at" in response["data"]["watchfolder"]["added_workflow"]
    assert "sha256" in response["data"]["watchfolder"]["added_workflow"]
    assert "added_variables" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["added_variables"] == variables

    assert "modified_workflow" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["modified_workflow"]["id"] == workflow.id
    assert "version" in response["data"]["watchfolder"]["modified_workflow"]
    assert "created_at" in response["data"]["watchfolder"]["modified_workflow"]
    assert "sha256" in response["data"]["watchfolder"]["modified_workflow"]
    assert "modified_variables" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["modified_variables"] == variables

    assert "deleted_workflow" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["deleted_workflow"]["id"] == workflow.id
    assert "version" in response["data"]["watchfolder"]["deleted_workflow"]
    assert "created_at" in response["data"]["watchfolder"]["deleted_workflow"]
    assert "sha256" in response["data"]["watchfolder"]["deleted_workflow"]
    assert "deleted_variables" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["deleted_variables"] == variables

    assert "added_items_key" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["added_items_key"] == "added_items_key"

    assert "modified_items_key" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["modified_items_key"] == "modified_items_key"

    assert "deleted_items_key" in response["data"]["watchfolder"]
    assert response["data"]["watchfolder"]["deleted_items_key"] == "deleted_items_key"

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_watchfolders(application: MainApplication, graphql_schema):
    """
    test_watchfolders

    :param application: Main Application instance
    :param graphql_schema: GraphQLSchema instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    workflow = application.workflows.create(workflow_content)

    # empty list
    query_ = """
    query ($workflow: WorkflowInput, $sort_column: WatchfolderSortColumn) {
        watchfolders (workflow: $workflow, sort_column: $sort_column) {
            count
            result {
                id
                status
            }
        }
    }
    """

    data: Dict[str, Any] = {
        "operationName": None,
        "variables": {"workflow": {"id": workflow.id}, "sort_column": "id"},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["watchfolders"]["count"] == 0
    assert len(response["data"]["watchfolders"]["result"]) == 0

    # 10 watchfolders
    for _ in range(0, 10):
        application.watchfolders.create("/tmp/madam_tests", added_workflow=workflow)

    query_ = """
    query ($workflow: WorkflowInput, $sort_column: WatchfolderSortColumn) {
        watchfolders (workflow: $workflow, sort_column: $sort_column) {
            count
            result {
                id
                status
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"workflow": {"id": workflow.id}, "sort_column": "id"},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["watchfolders"]["count"] == 10
    assert len(response["data"]["watchfolders"]["result"]) == 10
    assert "id" in response["data"]["watchfolders"]["result"][0]
    assert "status" in response["data"]["watchfolders"]["result"][0]
    assert (
        response["data"]["watchfolders"]["result"][0]["id"]
        < response["data"]["watchfolders"]["result"][9]["id"]
    )

    # Paginate forward with 5 elements per page
    query_ = """
    query ($offset: Int!, $limit: Int!) {
        watchfolders (offset: $offset, limit: $limit) {
            count
            result {
                id
                status
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"offset": 5, "limit": 5},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["watchfolders"]["count"] == 10
    assert len(response["data"]["watchfolders"]["result"]) == 5

    # Descending column
    query_ = """
    query ($sort_column: WatchfolderSortColumn!, $sort_ascending: Boolean!) {
        watchfolders (sort_column: $sort_column, sort_ascending: $sort_ascending) {
            count
            result {
                id
                start_at
            }
        }
    }
    """

    data = {
        "operationName": None,
        "variables": {"sort_column": "id", "sort_ascending": False},
        "query": query_,
    }
    context_value = {"application": application}
    success, response = graphql_sync(graphql_schema, data, context_value=context_value)
    assert success is True
    assert response["data"]["watchfolders"]["count"] == 10
    assert len(response["data"]["watchfolders"]["result"]) == 10
    assert (
        response["data"]["watchfolders"]["result"][0]["id"]
        > response["data"]["watchfolders"]["result"][9]["id"]
    )

    application.workflows.delete(workflow)
