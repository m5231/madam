# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Madam conftest fixtures module
"""
import logging
import os
import shutil
from pathlib import Path

import pytest
from ariadne import load_schema_from_path, make_executable_schema

from madam.adapters.repository.postgresql import PostgreSQLClient
from madam.domains.application import MainApplication
from madam.domains.entities.constants import MADAM_GRAPHQL_SCHEMA_PATH
from madam.slots.graphql.mutation import mutation
from madam.slots.graphql.query import query, timer_interface
from madam.slots.graphql.scalar import datetime_scalar, timedelta_scalar
from madam.slots.graphql.subscription import subscription, subscription_event_union_type

logging.basicConfig(level=os.getenv("MADAM_LOG_LEVEL", "ERROR").upper())

TESTS_CONFIG_PATH = str(Path(__file__).parents[1].joinpath("madam_tests.yaml"))
TESTS_TMP_PATH = "/tmp/madam_tests"


@pytest.fixture(name="tmp_folder")
def _tmp_folder():
    """
    Create tmp folder for tests files

    :return:
    """
    # create tmp folder
    if not Path(TESTS_TMP_PATH).exists():
        old_umask = os.umask(0o000)
        os.mkdir(TESTS_TMP_PATH, 0o777)
        os.umask(old_umask)

    return TESTS_TMP_PATH


@pytest.fixture(name="application")
def application_(tmp_folder):  # pylint: disable=unused-argument
    """
    Return Madam Application instance

    :return:
    """
    # create Madam application
    application = MainApplication(TESTS_CONFIG_PATH)

    # cleanup tables by cascading on workflows
    database_client = PostgreSQLClient(application.config)
    database_client.clear("workflows")

    yield application
    # TEARDOWN ##############################################

    # delete tmp folder
    shutil.rmtree(TESTS_TMP_PATH)


@pytest.fixture
def graphql_schema():
    """
    Return GraphQLSchema instance

    :return:
    """
    # construct schema from all *.graphql files in directory
    type_defs = load_schema_from_path(MADAM_GRAPHQL_SCHEMA_PATH)

    # create executable schema instance
    return make_executable_schema(
        type_defs,
        query,
        timer_interface,
        mutation,
        subscription,
        timedelta_scalar,
        subscription_event_union_type,
        datetime_scalar,
    )
