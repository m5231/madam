# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

from madam.libs import timecode


def test_timecode_seconds():
    """
    test_timecode_seconds
    :return:
    """
    seconds = 0
    assert str(timecode.TimecodeSeconds(seconds)) == "00:00:00.000"
    seconds = 3600 + 60 + 1.100
    assert str(timecode.TimecodeSeconds(seconds)) == "01:01:01.100"
    seconds = 23 * 3600 + 59 * 60 + 59.999
    assert str(timecode.TimecodeSeconds(seconds)) == "23:59:59.999"
