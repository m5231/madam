# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on timers domain module
"""
import datetime as dt
from datetime import datetime, timedelta, timezone
from pathlib import Path
from time import sleep
from typing import List, Union

from madam.domains.application import MainApplication
from madam.domains.entities.timer import STATUS_ABORTED as TIMER_STATUS_ABORTED
from madam.domains.entities.timer import STATUS_COMPLETE as TIMER_STATUS_COMPLETE
from madam.domains.entities.timer import STATUS_RUNNING as TIMER_STATUS_RUNNING
from madam.domains.entities.timer import CyclicTimer, DateTimer
from madam.domains.entities.workflow_instance import STATUS_COMPLETE, WorkflowInstance
from tests.conftest import TESTS_CONFIG_PATH

WORKFLOW_ID = "test_create_id"
WORKFLOW_TIMERS_ID = "timers"
WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/test_create.bpmn"
)
WORKFLOW_CYCLIC_TIMER_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/cyclic_timer.bpmn"
)
WORKFLOW_DATE_TIMER_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/date_timer.bpmn"
)


def test_create_date_timer(application: MainApplication):
    """
    Test create date timer

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    now_ = datetime.now()
    timer = application.timers.create_date_timer(workflow, now_, {"source": "/tmp"})

    assert timer.workflow.id == workflow.id
    assert timer.workflow.version == workflow.version
    assert timer.date == now_
    assert timer.status is TIMER_STATUS_RUNNING
    assert timer.start_at is not None
    assert timer.end_at is None
    assert timer.input == {"source": "/tmp"}

    application.workflows.delete(workflow)


def test_create_cyclic_timer(application: MainApplication):
    """
    Test create cyclic timer

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    timer = application.timers.create_cyclic_timer(
        workflow, 5, timedelta(1), {"source": "/tmp"}
    )

    assert timer.workflow.id == workflow.id
    assert timer.workflow.version == workflow.version
    assert timer.repeat == 5
    assert timer.interval == timedelta(1)
    assert timer.status == TIMER_STATUS_RUNNING
    assert timer.start_at is not None
    assert timer.end_at is None
    assert timer.input == {"source": "/tmp"}

    application.workflows.delete(workflow)


def test_read(application: MainApplication):
    """
    Test timers.read

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_DATE_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.repository.get_last_version_by_id(workflow.id) == 1
    assert workflow.version == 1

    date = datetime.fromtimestamp(
        datetime.now(tz=dt.timezone.utc).timestamp() + 86400, tz=dt.timezone.utc
    )
    timer_ = application.timers.create_date_timer(workflow, date, {"source": "/tmp"})

    timer = application.timers.read(timer_.id)
    assert isinstance(timer, DateTimer)
    assert timer_.status == timer.status
    assert timer_.workflow.id == timer.workflow.id
    assert timer_.workflow.version == timer.workflow.version
    assert timer_.start_at == timer.start_at
    assert timer_.end_at == timer.end_at
    assert timer_.input == timer.input
    assert timer_.date == timer.date

    application.workflows.delete(workflow)


def test_delete(application: MainApplication):
    """
    Test delete workflow

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    now_ = datetime.now()
    timer = application.timers.create_date_timer(workflow, now_, {"source": "/tmp"})

    timers = application.timers.list(status=TIMER_STATUS_RUNNING, workflow=workflow)
    assert len(timers) == 1

    application.timers.delete(timer)
    timers = application.timers.list(status=TIMER_STATUS_RUNNING, workflow=workflow)
    assert len(timers) == 0

    application.workflows.delete(workflow)


def test_list(application: MainApplication):
    """
    Test list workflows

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    now_ = datetime.now(timezone.utc)
    application.timers.create_date_timer(workflow, now_, {"source": "/tmp"})

    timers = application.timers.list(status=TIMER_STATUS_RUNNING, workflow=workflow)
    assert len(timers) == 1
    assert isinstance(timers[0], DateTimer)
    assert timers[0].date == now_

    application.workflows.delete(workflow)


def test_cyclic_timer_start_event(application: MainApplication):
    """
    Test cyclic timer start event

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_CYCLIC_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content_cyclic_timer = fh.read()

    workflow = application.workflows.create(workflow_content_cyclic_timer)
    assert workflow is not None
    assert workflow.timer == "R2/P1S"

    result = application.workflows.start(
        WORKFLOW_TIMERS_ID, variables={"source": "/tmp"}
    )
    assert result is True

    workflow_instances: List[WorkflowInstance] = []
    while len(workflow_instances) < 2:
        workflow_instances = application.workflow_instances.list(status=STATUS_COMPLETE)
        sleep(0.5)

    assert workflow_instances[0].workflow.id == workflow.id
    assert workflow_instances[1].workflow.id == workflow.id
    assert workflow_instances[0].workflow.version == workflow.version
    assert workflow_instances[1].workflow.version == workflow.version
    assert workflow_instances[0].status == STATUS_COMPLETE
    assert workflow_instances[1].status == STATUS_COMPLETE
    assert workflow_instances[0].end_at is not None
    assert workflow_instances[1].end_at is not None
    assert (
        workflow_instances[1].start_at - workflow_instances[0].start_at
    ).seconds == timedelta(seconds=1).seconds

    timers: List[Union[DateTimer, CyclicTimer]] = []
    while len(timers) < 1:
        timers = application.timers.list(status=TIMER_STATUS_COMPLETE)
        sleep(0.5)

    assert len(timers) == 1
    assert isinstance(timers[0], CyclicTimer)
    assert timers[0].repeat == 2
    assert timers[0].interval.seconds == 1
    assert timers[0].status == TIMER_STATUS_COMPLETE
    assert timers[0].end_at is not None

    application.workflows.delete(workflow)


def test_cyclic_timer_abort(application: MainApplication):
    """
    Test cyclic timer abort

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_CYCLIC_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content_cyclic_timer = fh.read()

    workflow = application.workflows.create(workflow_content_cyclic_timer)
    assert workflow is not None
    assert workflow.timer == "R2/P1S"

    result = application.workflows.start(
        WORKFLOW_TIMERS_ID, variables={"source": "/tmp"}
    )
    assert result is True

    sleep(1)
    application.workflows.abort(WORKFLOW_TIMERS_ID)

    timers: List[Union[DateTimer, CyclicTimer]] = []
    while len(timers) < 1:
        timers = application.timers.list(status=TIMER_STATUS_ABORTED)
        sleep(0.5)

    assert len(timers) == 1
    assert isinstance(timers[0], CyclicTimer)
    assert timers[0].repeat == 2
    assert timers[0].interval.seconds == 1
    assert timers[0].status == TIMER_STATUS_ABORTED
    assert timers[0].end_at is not None

    application.workflows.delete(workflow)


def test_date_timer_start_event(application: MainApplication):
    """
    Test date timer start event

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_DATE_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content_date_timer = fh.read()

    workflow = application.workflows.create(workflow_content_date_timer)
    assert workflow is not None
    assert workflow.timer == "2021-03-23T17:51:10+01:00"

    result = application.workflows.start(
        WORKFLOW_TIMERS_ID, variables={"source": "/tmp"}
    )
    assert result is True

    workflow_instances: List[WorkflowInstance] = []
    while len(workflow_instances) < 1:
        workflow_instances = application.workflow_instances.list(status=STATUS_COMPLETE)
        sleep(0.5)

    assert workflow_instances[0].workflow.id == workflow.id
    assert workflow_instances[0].workflow.version == workflow.version
    assert workflow_instances[0].status == STATUS_COMPLETE
    assert workflow_instances[0].end_at is not None
    assert workflow_instances[0].error is None

    timers: List[Union[DateTimer, CyclicTimer]] = []
    while len(timers) < 1:
        timers = application.timers.list(status=TIMER_STATUS_COMPLETE)
        sleep(0.5)

    assert len(timers) == 1
    assert isinstance(timers[0], DateTimer)
    assert timers[0].date == datetime.fromisoformat(workflow.timer)
    assert timers[0].status == TIMER_STATUS_COMPLETE
    assert timers[0].end_at is not None

    application.workflows.delete(workflow)


def test_date_timer_abort(application: MainApplication):
    """
    Test date timer start event

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_DATE_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content_date_timer = fh.read()

    date = datetime.fromtimestamp(
        datetime.now(tz=dt.timezone.utc).timestamp() + 86400, tz=dt.timezone.utc
    ).isoformat()

    workflow_content_date_timer_abort = workflow_content_date_timer.replace(
        "2021-03-23T17:51:10+01:00", date
    )

    workflow = application.workflows.create(workflow_content_date_timer_abort)
    assert workflow is not None

    result = application.workflows.start(
        WORKFLOW_TIMERS_ID, variables={"source": "/tmp"}
    )
    assert result is True

    sleep(1)
    application.workflows.abort(WORKFLOW_TIMERS_ID)

    timers: List[Union[DateTimer, CyclicTimer]] = []
    while len(timers) < 1:
        timers = application.timers.list(status=TIMER_STATUS_ABORTED)
        sleep(0.5)

    assert len(timers) == 1
    assert isinstance(timers[0], DateTimer)
    assert timers[0].date.isoformat() == workflow.timer
    assert timers[0].status == TIMER_STATUS_ABORTED
    assert timers[0].end_at is not None

    application.workflows.delete(workflow)


def test_restart_running_timer(application: MainApplication):
    """
    Test that running timers in database are restarted in Timers domain init

    :param application: Application instance
    :return:
    """
    with open(WORKFLOW_DATE_TIMER_PATH, "r", encoding="utf-8") as fh:
        workflow_content_date_timer = fh.read()

    date = datetime.fromtimestamp(
        datetime.now(tz=dt.timezone.utc).timestamp() + 86400, tz=dt.timezone.utc
    ).isoformat()

    workflow_content_date_timer_abort = workflow_content_date_timer.replace(
        "2021-03-23T17:51:10+01:00", date
    )

    workflow = application.workflows.create(workflow_content_date_timer_abort)
    assert workflow is not None

    result = application.workflows.start(
        WORKFLOW_TIMERS_ID, variables={"source": "/tmp"}
    )
    assert result is True

    # create new Madam application domain
    restarted_application = MainApplication(TESTS_CONFIG_PATH)

    timers = restarted_application.timers.list(
        status=TIMER_STATUS_RUNNING, workflow=workflow
    )
    assert len(timers) == 1
    assert timers[0] == list(restarted_application.timers.register.values())[0].timer

    sleep(1)
    application.workflows.abort(workflow.id, workflow.version)
    restarted_application.workflows.abort(workflow.id, workflow.version)
    sleep(1)
    restarted_application.workflows.delete(workflow)
