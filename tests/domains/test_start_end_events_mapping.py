# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on workflows domain module
"""
from pathlib import Path
from time import sleep

from madam.domains.application import MainApplication
from madam.domains.entities.workflow_instance import STATUS_COMPLETE, STATUS_RUNNING

WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/start_end_events_mapping.bpmn"
)


def test_start_end_events_mapping(application: MainApplication):
    """
    test_start_end_events_mapping

    :param application: Madam main Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_COMPLETE
    assert instances[0].end_at is not None
    assert instances[0].input is None
    assert instances[0].output is not None
    assert instances[0].error is None

    # check global variable created by start event output mapping
    assert "destination" in instances[0].output

    # check global variable created by end event output mapping in a subprocess
    assert "source" in instances[0].output

    # check global variable created by last service output variable in main process
    assert "mediainfo" in instances[0].output

    # check global variable created by end event output mapping in main process
    assert "results" in instances[0].output

    application.workflows.delete(workflow)
