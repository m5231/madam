# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path

from madam.adapters.bpmn.parser import ZeebeeBPMNXMLParser

WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/concatenate/concatenate.bpmn"
)
WORKFLOW_PATH_SUBPROCESS = Path(__file__).parent.parent.joinpath(
    "assets/workflows/ffmpeg_chunked.bpmn"
)


def test_get_service_types():
    """
    test_get_service_types
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    bpmn_parser = ZeebeeBPMNXMLParser()
    services = bpmn_parser.get_service_types(workflow_content)
    assert services == ["ffmpeg", "concatenate"]

    with open(WORKFLOW_PATH_SUBPROCESS, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    services = bpmn_parser.get_service_types(workflow_content)
    assert services == ["scenes", "ffmpeg", "concatenate", "ffmpeg", "ffmpeg", "ffmpeg"]
