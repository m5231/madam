# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests fixed chunk video encoding with ffmpeg agent
"""
from pathlib import Path
from time import sleep

import docker
from docker.errors import ContainerError
from madam.domains.application import MainApplication
from madam.domains.entities.application import (
    STATUS_COMPLETE as APPLICATION_STATUS_COMPLETE,
)
from madam.domains.entities.job import STATUS_COMPLETE as JOB_STATUS_COMPLETE
from madam.domains.entities.workflow_instance import STATUS_COMPLETE, STATUS_RUNNING

FFMPEG_CHUNKED_WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/ffmpeg_fixed_chunks.bpmn"
)


# pylint: disable=too-many-statements
def test_ffmpeg_fixed_chunks_workflow(application: MainApplication):
    """
    Test FFmpeg fixed chunks video encoding

    :param application: Madam Application instance
    :return:
    """
    with open(FFMPEG_CHUNKED_WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_RUNNING)) == 1:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)

    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_COMPLETE
    assert instances[0].input is None
    assert isinstance(instances[0].output, dict)
    assert instances[0].end_at is not None
    assert instances[0].error is None

    jobs = application.jobs.list(status=JOB_STATUS_COMPLETE)
    assert len(jobs) == 18

    applications = application.applications.list(status=APPLICATION_STATUS_COMPLETE)
    assert len(applications) == 15

    # check sources are created
    assert Path("/tmp/madam_tests/video_01.mkv").exists()
    assert Path("/tmp/madam_tests/video_02.mkv").exists()
    assert Path("/tmp/madam_tests/video_03.mkv").exists()

    client = docker.DockerClient()
    docker_config = application.config.get_root_key("docker")
    # extract all images from source video
    client.containers.run(
        image=docker_config["applications"]["ffmpeg"]["latest"],
        command="-y -i /tmp/madam_tests/video_01.mkv -f image2 /tmp/madam_tests/image_source_%04d.jpg",
        volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
        remove=True,
    )

    # check source first chunk split
    stderr = ""
    try:
        client.containers.run(
            image=docker_config["applications"]["imagemagick"]["latest"],
            entrypoint="compare",
            command="-metric FUZZ /tmp/madam_tests/image_source_0054.jpg \
            /tmp/madam_tests/image_source_0055.jpg /dev/null",
            volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
            remove=True,
        )
    except ContainerError as exception:
        # compare -metric return exit code 0 if image are exactly the same
        # 1 if there is a difference (so we need to catch the exception)
        # 2 if error
        stderr = exception.stderr.decode("utf-8")
    assert float(stderr.split()[0]) > 30000

    # check source second chunk split
    stderr = ""
    try:
        client.containers.run(
            image=docker_config["applications"]["imagemagick"]["latest"],
            entrypoint="compare",
            command="-metric FUZZ /tmp/madam_tests/image_source_0132.jpg \
            /tmp/madam_tests/image_source_0133.jpg /dev/null",
            volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
            remove=True,
        )
    except ContainerError as exception:
        # compare -metric return exit code 0 if image are exactly the same
        # 1 if there is a difference (so we need to catch the exception)
        # 2 if error
        stderr = exception.stderr.decode("utf-8")
    assert float(stderr.split()[0]) > 30000

    # check split chunks are created
    assert Path("/tmp/madam_tests/video_01_chunk_0000.mp4").exists()
    assert Path("/tmp/madam_tests/video_01_chunk_0001.mp4").exists()
    assert Path("/tmp/madam_tests/video_01_chunk_0002.mp4").exists()
    assert Path("/tmp/madam_tests/video_02_chunk_0000.mp4").exists()
    assert Path("/tmp/madam_tests/video_02_chunk_0001.mp4").exists()
    assert Path("/tmp/madam_tests/video_02_chunk_0002.mp4").exists()
    assert Path("/tmp/madam_tests/video_03_chunk_0000.mp4").exists()
    assert Path("/tmp/madam_tests/video_03_chunk_0001.mp4").exists()
    assert Path("/tmp/madam_tests/video_03_chunk_0002.mp4").exists()

    # check duration of chunk 00 in frames
    frames = client.containers.run(
        image=docker_config["applications"]["ffmpeg"]["latest"],
        entrypoint="",
        command="ffprobe -v error -select_streams v:0 -count_frames -show_entries \
        stream=nb_read_frames -print_format default=nokey=1:noprint_wrappers=1 \
        /tmp/madam_tests/video_01_chunk_0000.mp4",
        volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
        remove=True,
    )
    assert int(frames.strip()) == 54

    # check duration of chunk 01 in frames
    frames = client.containers.run(
        image=docker_config["applications"]["ffmpeg"]["latest"],
        entrypoint="",
        command="ffprobe -v error -select_streams v:0 -count_frames -show_entries \
        stream=nb_read_frames -print_format default=nokey=1:noprint_wrappers=1 \
        /tmp/madam_tests/video_01_chunk_0001.mp4",
        volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
        remove=True,
    )
    assert int(frames.strip()) == 78

    # check duration of chunk 02 in frames
    frames = client.containers.run(
        image=docker_config["applications"]["ffmpeg"]["latest"],
        entrypoint="",
        command="ffprobe -v error -select_streams v:0 -count_frames -show_entries \
        stream=nb_read_frames -print_format default=nokey=1:noprint_wrappers=1 \
        /tmp/madam_tests/video_01_chunk_0002.mp4",
        volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
        remove=True,
    )
    assert int(frames.strip()) == 103

    # check encoded audio tracks are created
    assert Path("/tmp/madam_tests/video_01_audio.mkv").exists()
    assert Path("/tmp/madam_tests/video_02_audio.mkv").exists()
    assert Path("/tmp/madam_tests/video_03_audio.mkv").exists()

    # check encoded video tracks are created
    assert Path("/tmp/madam_tests/video_01_video.mp4").exists()
    assert Path("/tmp/madam_tests/video_02_video.mp4").exists()
    assert Path("/tmp/madam_tests/video_03_video.mp4").exists()

    # check encoded video are created
    assert Path("/tmp/madam_tests/video_01_encoded.mp4").exists()
    assert Path("/tmp/madam_tests/video_02_encoded.mp4").exists()
    assert Path("/tmp/madam_tests/video_03_encoded.mp4").exists()

    # check duration of encoded video in frames
    frames = client.containers.run(
        image=docker_config["applications"]["ffmpeg"]["latest"],
        entrypoint="",
        command="ffprobe -v error -select_streams v:0 -count_frames -show_entries \
        stream=nb_read_frames -print_format default=nokey=1:noprint_wrappers=1 \
        /tmp/madam_tests/video_01_encoded.mp4",
        volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
        remove=True,
    )
    assert int(frames.strip()) == 235

    # extract all images from encoded video
    client.containers.run(
        image=docker_config["applications"]["ffmpeg"]["latest"],
        command="-y -i /tmp/madam_tests/video_01_encoded.mp4 -f image2 /tmp/madam_tests/image_encoded_%04d.jpg",
        volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
        remove=True,
    )

    # check encoded first chunk split
    stderr = ""
    try:
        client.containers.run(
            image=docker_config["applications"]["imagemagick"]["latest"],
            entrypoint="compare",
            command="-metric FUZZ /tmp/madam_tests/image_encoded_0054.jpg \
            /tmp/madam_tests/image_encoded_0055.jpg /dev/null",
            volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
            remove=True,
        )
    except ContainerError as exception:
        # compare -metric return exit code 0 if image are exactly the same
        # 1 if there is a difference (so we need to catch the exception)
        # 2 if error
        stderr = exception.stderr.decode("utf-8")
    assert float(stderr.split()[0]) > 30000

    # check encoded second chunk split
    stderr = ""
    try:
        client.containers.run(
            image=docker_config["applications"]["imagemagick"]["latest"],
            entrypoint="compare",
            command="-metric FUZZ /tmp/madam_tests/image_encoded_0132.jpg \
            /tmp/madam_tests/image_encoded_0133.jpg /dev/null",
            volumes={"/tmp/madam_tests": {"bind": "/tmp/madam_tests", "mode": "rw"}},
            remove=True,
        )
    except ContainerError as exception:
        # compare -metric return exit code 0 if image are exactly the same
        # 1 if there is a difference (so we need to catch the exception)
        # 2 if error
        stderr = exception.stderr.decode("utf-8")
    assert float(stderr.split()[0]) > 30000

    application.workflows.delete(workflow)
