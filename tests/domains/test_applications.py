# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on applications domain module
"""
from pathlib import Path

from docker.errors import APIError
from madam.domains.application import MainApplication
from madam.domains.entities.application import STATUS_ERROR
from madam.domains.entities.job import STATUS_RUNNING

WORKFLOW_ID = "test_create_id"
WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/test_create.bpmn"
)
AGENT_ID = "ffmpeg_create_video"
AGENT_TYPE = "ffmpeg"
APPLICATION_NAME = "ffmpeg"
APPLICATION_VERSION = "latest"
APPLICATION_ARGUMENTS = "-y -i /path"


def test_create(application: MainApplication):
    """
    Test applications.create

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)
    job = application.jobs.create(
        workflow_instance, AGENT_ID, AGENT_TYPE, {"arguments": "-y -i /path"}, {}
    )
    application_ = application.applications.create(
        job, APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_ARGUMENTS
    )

    assert application_.job.id == job.id
    assert application_.status is STATUS_RUNNING
    assert application_.name == APPLICATION_NAME
    assert application_.logs is None
    assert application_.arguments == APPLICATION_ARGUMENTS
    assert application_.container_id is None
    assert application_.container_name.startswith(APPLICATION_NAME)
    assert application_.container_name == f"{application_.name}_{application_.id}"
    assert application_.container_error is None
    assert job.start_at is not None
    assert job.end_at is None

    application.workflows.delete(workflow)


def test_read(application: MainApplication):
    """
    Test applications.read

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)
    job = application.jobs.create(
        workflow_instance, AGENT_ID, AGENT_TYPE, {"arguments": "-y -i /path"}, {}
    )
    application_ = application.applications.create(
        job, "app_name", "app_version", "arguments"
    )

    application_read = application.applications.read(application_.id)
    assert application_.status == application_read.status
    assert application_.job.id == application_read.job.id
    assert application_.start_at == application_read.start_at
    assert application_.end_at == application_read.end_at
    assert application_.name == application_read.name
    assert application_.arguments == application_read.arguments
    assert application_.container_id == application_read.container_id
    assert application_.container_name == application_read.container_name
    assert application_.container_error == application_read.container_error
    assert application_.logs == application_read.logs

    application.workflows.delete(workflow)


def test_list(application: MainApplication):
    """
    Test list applications

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)

    job = application.jobs.create(
        workflow_instance,
        "agent_ffmpeg",
        "ffmpeg",
        {"arguments": "-y -i {{source}}"},
        {"source": "/path/filename"},
    )

    # empty list
    applications = application.applications.list(job_id=job.id, status=STATUS_RUNNING)
    assert len(applications) == 0

    # 10 applications
    for index in range(0, 10):
        application.applications.create(
            job, f"application_ffmpeg_{index}", APPLICATION_VERSION, "-y -i {{source}}"
        )

    applications = application.applications.list(job_id=job.id, status=STATUS_RUNNING)
    assert len(applications) == 10
    assert applications[0].name == "application_ffmpeg_0"
    assert applications[9].name == "application_ffmpeg_9"
    assert applications[0].start_at < applications[9].start_at

    # Paginate forward with 5 elements per page
    applications = application.applications.list(
        offset=5, limit=5, job_id=job.id, status=STATUS_RUNNING
    )
    assert len(applications) == 5
    assert applications[0].name == "application_ffmpeg_5"
    assert applications[4].name == "application_ffmpeg_9"
    assert applications[0].start_at < applications[4].start_at

    # Descending column
    applications = application.applications.list(
        sort_column="start_at",
        sort_ascending=False,
        job_id=job.id,
        status=STATUS_RUNNING,
    )
    assert len(applications) == 10
    assert applications[9].name == "application_ffmpeg_0"
    assert applications[0].name == "application_ffmpeg_9"
    assert applications[0].start_at > applications[9].start_at

    application.workflows.delete(workflow)


def test_delete(application: MainApplication):
    """
    Test delete application

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(
        workflow, {"source": "/tmp"}
    )
    job = application.jobs.create(
        workflow_instance, AGENT_ID, AGENT_TYPE, {"arguments": "-y -i /path"}, {}
    )
    application.applications.create(
        job, APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_ARGUMENTS
    )

    applications = application.applications.list(job_id=job.id, status=STATUS_RUNNING)
    assert len(applications) == 1

    application.applications.delete(applications[0])

    applications = application.applications.list(job_id=job.id, status=STATUS_RUNNING)
    assert len(applications) == 0

    application.workflows.delete(workflow)


def test_run_process_failure(application: MainApplication, mocker):
    """
    Test applications.run

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)
    job = application.jobs.create(
        workflow_instance, AGENT_ID, AGENT_TYPE, {"arguments": "-y -i /path"}, {}
    )

    # Mock to raise Exception in process manager
    docker_error_message = "503 Server Error"
    mocker.patch.object(
        application.applications.process_manager,
        "run",
        side_effect=APIError(docker_error_message),
    )

    application_ = application.applications.run(
        job, "app_name", "app_version", "arguments"
    )

    assert application_.status == STATUS_ERROR
    assert application_.end_at is not None
    assert application_.container_id is None
    assert application_.container_error == docker_error_message

    application.workflows.delete(workflow)
