# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on workflows domain module
"""
from pathlib import Path

WORKFLOW_ID = "test_create_id"
WORKFLOW_TIMERS_ID = "timers"
WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/test_create.bpmn"
)
WORKFLOW_CYCLIC_TIMER_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/cyclic_timer.bpmn"
)
WORKFLOW_DATE_TIMER_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/date_timer.bpmn"
)


def test_create(application):
    """
    Test create workflow

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content_01 = fh.read()

    # create three versions of the same workflow id
    workflow_content_02 = workflow_content_01.replace(
        "Test Create Name 01", "Test Create Name 02"
    )
    workflow_content_03 = workflow_content_01.replace(
        "Test Create Name 01", "Test Create Name 03"
    )

    workflow_01 = application.workflows.create(workflow_content_01)
    workflow_02 = application.workflows.create(workflow_content_02)
    workflow_03 = application.workflows.create(workflow_content_03)

    assert application.workflows.repository.get_last_version_by_id(WORKFLOW_ID) == 3
    assert workflow_01.version == 1
    assert workflow_02.version == 2
    assert workflow_03.version == 3

    application.workflows.delete(workflow_01)
    application.workflows.delete(workflow_02)
    application.workflows.delete(workflow_03)


def test_read(application):
    """
    Test read workflow

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content_01 = fh.read()

    # create three versions of the same workflow id
    workflow_content_02 = workflow_content_01.replace(
        "Test Create Name 01", "Test Create Name 02"
    )

    workflow_01 = application.workflows.create(workflow_content_01)
    workflow_02 = application.workflows.create(workflow_content_02)

    assert application.workflows.repository.get_last_version_by_id(WORKFLOW_ID) == 2

    workflow = application.workflows.read(WORKFLOW_ID, 1)
    assert workflow.version == 1
    assert workflow.name == "Test Create Name 01"
    assert workflow.sha256 == workflow_01.sha256

    workflow = application.workflows.read(WORKFLOW_ID)
    assert workflow.version == 2
    assert workflow.name == "Test Create Name 02"
    assert workflow.sha256 == workflow_02.sha256

    application.workflows.delete(workflow_01)
    application.workflows.delete(workflow_02)


def test_delete(application):
    """
    Test delete workflow

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content_01 = fh.read()

    workflow = application.workflows.create(workflow_content_01)

    application.workflows.delete(workflow)
    assert application.workflows.repository.get_last_version_by_id(WORKFLOW_ID) == 0


def test_list(application):
    """
    Test list workflows

    :param application: Madam Application instance
    :return:
    """
    # empty list
    workflows = application.workflows.list()
    assert len(workflows) == 0

    # 10 workflows
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    # create nine other versions of the same workflow id
    for index in range(2, 11):
        workflow_content_tmp = workflow_content.replace(
            "Test Create Name 01", f"Test Create Name {index:02d}"
        )
        application.workflows.create(workflow_content_tmp)

    workflows = application.workflows.list()
    assert len(workflows) == 10
    assert (
        workflows[0].sha256
        == "5d0838d15fa8c7035bd352ad7dff9e46c95edd6dda9a18f522976ce28b911aa1"
    )
    assert workflows[0].version == 1
    assert (
        workflows[9].sha256
        == "6d7d4125633a5aeb98c8a1be7dff0b8d24807b390619b78dcb14eed3fe5784fc"
    )
    assert workflows[9].version == 10

    # Paginate forward with 5 elements per page
    workflows = application.workflows.list(offset=5, limit=5)
    assert len(workflows) == 5
    assert (
        workflows[0].sha256
        == "2c86f97d2aa9b613c0bbf12128427e1242f388049d7eaad6ce786f9959bcdd7a"
    )
    assert workflows[0].version == 6
    assert (
        workflows[4].sha256
        == "6d7d4125633a5aeb98c8a1be7dff0b8d24807b390619b78dcb14eed3fe5784fc"
    )
    assert workflows[4].version == 10

    # Descending order
    workflows = application.workflows.list(sort_column="version", sort_ascending=False)
    assert len(workflows) == 10
    assert (
        workflows[0].sha256
        == "6d7d4125633a5aeb98c8a1be7dff0b8d24807b390619b78dcb14eed3fe5784fc"
    )
    assert workflows[0].version == 10
    assert (
        workflows[9].sha256
        == "5d0838d15fa8c7035bd352ad7dff9e46c95edd6dda9a18f522976ce28b911aa1"
    )
    assert workflows[9].version == 1


def test_count(application):
    """
    Test count workflows

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content_01 = fh.read()

    # create three versions of the same workflow id
    workflow_content_02 = workflow_content_01.replace(
        "Test Create Name 01", "Test Create Name 02"
    )

    workflow_01 = application.workflows.create(workflow_content_01)
    workflow_02 = application.workflows.create(workflow_content_02)

    assert application.workflows.repository.get_last_version_by_id(WORKFLOW_ID) == 2

    count = application.workflows.count()
    assert count == 2
    count = application.workflows.count(last_version=True)
    assert count == 1

    application.workflows.delete(workflow_01)
    application.workflows.delete(workflow_02)
