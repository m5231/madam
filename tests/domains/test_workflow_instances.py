# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on workflow_instances domain module
"""
import shutil
from pathlib import Path
from time import sleep

from madam.domains.entities.workflow_instance import STATUS_COMPLETE, STATUS_RUNNING

WORKFLOW_ID = "test_create_id"
WORKFLOW_TIMERS_ID = "timers"
WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/test_create.bpmn"
)
WORKFLOW_PATH_PYTHON_SLEEP = Path(__file__).parent.parent.joinpath(
    "assets/python/python_sleep.bpmn"
)
PYTHON_SLEEP_PATH = Path(__file__).parent.parent.joinpath("assets/python/sleep.py")
WORKFLOW_CYCLIC_TIMER_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/cyclic_timer.bpmn"
)
WORKFLOW_DATE_TIMER_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/date_timer.bpmn"
)


def test_create(application):
    """
    Test workflow_instances.create

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.repository.get_last_version_by_id(WORKFLOW_ID) == 1
    assert workflow.version == 1

    workflow_instance = application.workflow_instances.create(workflow)
    assert workflow_instance.status == STATUS_RUNNING
    assert workflow_instance.workflow == workflow
    assert workflow_instance.start_at is not None
    assert workflow_instance.end_at is None
    assert workflow_instance.input is None
    assert workflow_instance.output is None
    assert workflow_instance.error is None

    application.workflows.delete(workflow)


def test_read(application):
    """
    Test workflow_instances.read

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    assert application.workflows.repository.get_last_version_by_id(WORKFLOW_ID) == 1
    assert workflow.version == 1

    workflow_instance = application.workflow_instances.create(workflow)

    workflow_instance_read = application.workflow_instances.read(workflow_instance.id)
    assert workflow_instance.status == workflow_instance_read.status
    assert workflow_instance.workflow.id == workflow_instance_read.workflow.id
    assert workflow_instance.workflow.version == workflow_instance_read.workflow.version
    assert workflow_instance.start_at == workflow_instance_read.start_at
    assert workflow_instance.end_at == workflow_instance_read.end_at
    assert workflow_instance.input == workflow_instance_read.input
    assert workflow_instance.output == workflow_instance_read.output
    assert workflow_instance.error is None

    application.workflows.delete(workflow)


def test_list(application):
    """
    Test list workflow instances

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    workflow = application.workflows.create(workflow_content)

    # empty list
    instances = application.workflow_instances.list(workflow=workflow)
    assert len(instances) == 0

    # 10 instances
    for _ in range(0, 10):
        application.workflow_instances.create(workflow)

    instances = application.workflow_instances.list(workflow=workflow)
    assert len(instances) == 10
    assert instances[0].start_at < instances[9].start_at
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_RUNNING
    assert instances[0].input is None
    assert instances[0].output is None
    assert instances[0].start_at is not None
    assert instances[0].end_at is None
    assert instances[0].error is None

    # Paginate forward with 5 elements per page
    instances = application.workflow_instances.list(
        offset=5, limit=5, workflow=workflow
    )
    assert len(instances) == 5
    assert instances[0].start_at < instances[4].start_at

    # Descending column
    instances = application.workflow_instances.list(
        sort_column="start_at", sort_ascending=False, workflow=workflow
    )
    assert len(instances) == 10
    assert instances[0].start_at > instances[9].start_at

    application.workflows.delete(workflow)


def test_delete(application):
    """
    Test delete workflow instance

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    application.workflows.start(workflow.id, workflow.version, {"source": "/tmp"})

    sleep(1)
    instances = application.workflow_instances.list()
    assert len(instances) == 1

    application.workflow_instances.delete(instances[0])

    sleep(1)
    instances = application.workflow_instances.list()
    assert len(instances) == 0

    application.workflows.delete(workflow)


def test_parallelism(application):
    """
    Test run multiple workflow instances in parallel

    :param application: Madam Application instance
    :return:
    """
    # copy python code to madam_tests folder
    shutil.copy(PYTHON_SLEEP_PATH, "/tmp/madam_tests")

    with open(WORKFLOW_PATH_PYTHON_SLEEP, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)

    result = application.workflows.start(workflow.id)
    assert result is True
    result = application.workflows.start(workflow.id)
    assert result is True
    result = application.workflows.start(workflow.id)
    assert result is True

    while len(application.workflow_instances.list(status=STATUS_COMPLETE)) < 3:
        sleep(0.5)

    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 3
    assert instances[0].workflow.id == workflow.id
    assert instances[0].workflow.version == workflow.version
    assert instances[0].status == STATUS_COMPLETE
    assert instances[0].input is None
    assert isinstance(instances[0].output, dict)
    assert instances[0].end_at is not None
    assert instances[0].error is None

    # check that instances are simultaneously executed
    assert (instances[0].start_at - instances[1].start_at).total_seconds() < 1
    assert (instances[0].start_at - instances[2].start_at).total_seconds() < 1
    for index in range(0, 3):
        assert (
            instances[index].end_at - instances[index].start_at
        ).total_seconds() < 11

    application.workflows.delete(workflow)


def test_sequential(application):
    """
    Test sequential start of workflow instance

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    variables = {"source": "/tmp"}
    application.workflows.start(workflow.id, workflow.version, variables)

    sleep(1)
    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 1
    assert instances[0].input == {"source": "/tmp"}

    application.workflows.start(workflow.id, workflow.version, variables)

    sleep(1)
    instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(instances) == 2
    assert instances[0].input == {"source": "/tmp"}
    assert instances[1].input == {"source": "/tmp"}

    application.workflows.delete(workflow)
