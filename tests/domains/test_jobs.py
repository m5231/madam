# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on jobs domain module
"""
from pathlib import Path

from madam.domains.entities.job import STATUS_RUNNING

WORKFLOW_ID = "test_create_id"
WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/test_create.bpmn"
)
AGENT_ID = "ffmpeg_create_video"
AGENT_TYPE = "ffmpeg"


def test_create(application):
    """
    Test jobs.create

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)
    job = application.jobs.create(
        workflow_instance, AGENT_ID, AGENT_TYPE, {"arguments": "-y -i /path"}, {}
    )

    assert job.status is STATUS_RUNNING
    assert job.workflow_instance.id == workflow_instance.id
    assert job.headers == {"arguments": "-y -i /path"}
    assert job.input == {}
    assert job.output is None
    assert job.agent_id == AGENT_ID
    assert job.agent_type == AGENT_TYPE
    assert job.start_at is not None
    assert job.end_at is None
    assert job.error is None

    application.workflows.delete(workflow)


def test_read(application):
    """
    Test jobs.read

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)
    job_ = application.jobs.create(
        workflow_instance, AGENT_ID, AGENT_TYPE, {"arguments": "-y -i /path"}, {}
    )

    job = application.jobs.read(job_.id)
    assert job_.status == job.status
    assert job_.workflow_instance.id == job.workflow_instance.id
    assert job_.start_at == job.start_at
    assert job_.end_at == job.end_at
    assert job_.input == job.input
    assert job_.output == job.output
    assert job_.headers == job.headers
    assert job_.agent_id == job.agent_id
    assert job_.agent_type == job.agent_type
    assert job_.error == job.error

    application.workflows.delete(workflow)


def test_list(application):
    """
    Test list jobs

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(workflow)

    # empty list
    jobs = application.jobs.list(
        workflow_instance_id=workflow_instance.id, status=STATUS_RUNNING
    )
    assert len(jobs) == 0

    # 10 jobs
    for index in range(0, 10):
        application.jobs.create(
            workflow_instance,
            f"agent_ffmpeg_{index}",
            "ffmpeg",
            {"arguments": "-y -i {{source}}"},
            {"source": "/path/filename"},
        )

    jobs = application.jobs.list(
        workflow_instance_id=workflow_instance.id, status=STATUS_RUNNING
    )
    assert len(jobs) == 10
    assert jobs[0].agent_id == "agent_ffmpeg_0"
    assert jobs[9].agent_id == "agent_ffmpeg_9"
    assert jobs[0].start_at < jobs[9].start_at

    # Paginate forward with 5 elements per page
    jobs = application.jobs.list(
        offset=5,
        limit=5,
        workflow_instance_id=workflow_instance.id,
        status=STATUS_RUNNING,
    )
    assert len(jobs) == 5
    assert jobs[0].agent_id == "agent_ffmpeg_5"
    assert jobs[4].agent_id == "agent_ffmpeg_9"
    assert jobs[0].start_at < jobs[4].start_at

    # Descending column
    jobs = application.jobs.list(
        sort_column="start_at",
        sort_ascending=False,
        workflow_instance_id=workflow_instance.id,
        status=STATUS_RUNNING,
    )
    assert len(jobs) == 10
    assert jobs[9].agent_id == "agent_ffmpeg_0"
    assert jobs[0].agent_id == "agent_ffmpeg_9"
    assert jobs[0].start_at > jobs[9].start_at

    application.workflows.delete(workflow)


def test_delete(application):
    """
    Test delete job

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    workflow_instance = application.workflow_instances.create(
        workflow, {"source": "/tmp"}
    )
    application.jobs.create(
        workflow_instance, AGENT_ID, AGENT_TYPE, {"arguments": "-y -i /path"}, {}
    )

    jobs = application.jobs.list(
        workflow_instance_id=workflow_instance.id, status=STATUS_RUNNING
    )
    assert len(jobs) == 1

    application.jobs.delete(jobs[0])

    jobs = application.jobs.list(
        workflow_instance_id=workflow_instance.id, status=STATUS_RUNNING
    )
    assert len(jobs) == 0

    application.workflows.delete(workflow)
