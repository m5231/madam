# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on watchfolders domain module
"""
import os
from datetime import datetime
from pathlib import Path
from time import sleep

from madam.domains.application import MainApplication
from madam.domains.entities.watchfolder import (
    STATUS_RUNNING as WATCHFOLDER_STATUS_RUNNING,
)
from madam.domains.entities.watchfolder import (
    STATUS_STOPPED as WATCHFOLDER_STATUS_STOPPED,
)
from madam.domains.entities.workflow_instance import STATUS_COMPLETE

WORKFLOW_PATH = Path(__file__).parent.parent.joinpath(
    "assets/workflows/test_create.bpmn"
)
ADDED_FILE_PATH = "/tmp/madam_tests/added_file.txt"


def test_create(application: MainApplication):
    """
    Test create watchfolder

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    variables = {"source": "/tmp"}
    watchfolder = application.watchfolders.create(
        path="/tmp/madam_tests",
        re_files=".+.mp4",
        re_dirs="test_.+",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
    )

    assert watchfolder.status is WATCHFOLDER_STATUS_STOPPED
    assert watchfolder.start_at is None
    assert watchfolder.end_at is None
    assert watchfolder.re_files == ".+.mp4"
    assert watchfolder.re_dirs == "test_.+"
    assert watchfolder.added_workflow == workflow
    assert watchfolder.added_variables == variables
    assert watchfolder.modified_workflow == workflow
    assert watchfolder.modified_variables == variables
    assert watchfolder.added_workflow == workflow
    assert watchfolder.added_variables == variables
    assert watchfolder.added_items_key == "added_items"
    assert watchfolder.modified_items_key == "modified_items"
    assert watchfolder.deleted_items_key == "deleted_items"

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_read(application: MainApplication):
    """
    Test timers.read

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    variables = {"source": "/tmp"}
    watchfolder_ = application.watchfolders.create(
        path="/tmp/madam_tests",
        re_files=".+.mp4",
        re_dirs="test_.+",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
    )

    watchfolder = application.watchfolders.read(watchfolder_.id)
    assert watchfolder.status == watchfolder_.status
    assert watchfolder.start_at == watchfolder_.start_at
    assert watchfolder.end_at == watchfolder_.end_at
    assert watchfolder.re_files == watchfolder_.re_files
    assert watchfolder.re_dirs == watchfolder_.re_dirs
    assert watchfolder.added_workflow == watchfolder_.added_workflow
    assert watchfolder.added_variables == watchfolder_.added_variables
    assert watchfolder.modified_workflow == watchfolder_.added_workflow
    assert watchfolder.modified_variables == watchfolder_.added_variables
    assert watchfolder.deleted_workflow == watchfolder_.added_workflow
    assert watchfolder.deleted_variables == watchfolder_.added_variables
    assert watchfolder.added_items_key == watchfolder_.added_items_key
    assert watchfolder.modified_items_key == watchfolder_.modified_items_key
    assert watchfolder.deleted_items_key == watchfolder_.deleted_items_key

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_update(application: MainApplication):
    """
    Test update watchfolder

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    variables = {"source": "/tmp"}
    watchfolder = application.watchfolders.create(
        path="/tmp/madam_tests",
        re_files=".+.mp4",
        re_dirs="test_.+",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
    )

    assert watchfolder.status is WATCHFOLDER_STATUS_STOPPED
    assert watchfolder.start_at is None
    assert watchfolder.end_at is None
    assert watchfolder.re_files == ".+.mp4"
    assert watchfolder.re_dirs == "test_.+"
    assert watchfolder.added_workflow == workflow
    assert watchfolder.added_variables == variables
    assert watchfolder.modified_workflow == workflow
    assert watchfolder.modified_variables == variables
    assert watchfolder.added_workflow == workflow
    assert watchfolder.added_variables == variables
    assert watchfolder.added_items_key == "added_items"
    assert watchfolder.modified_items_key == "modified_items"
    assert watchfolder.deleted_items_key == "deleted_items"

    application.watchfolders.update(
        watchfolder,
        path="/tmp/madam_tests",
        re_files=".+.mp4",
        re_dirs="test_.+",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
        status=WATCHFOLDER_STATUS_STOPPED,
        end_at=datetime.now(),
    )

    watchfolder_ = application.watchfolders.read(watchfolder.id)
    assert watchfolder.status == watchfolder_.status
    assert watchfolder.start_at == watchfolder_.start_at
    assert watchfolder.end_at == watchfolder_.end_at
    assert watchfolder.re_files == watchfolder_.re_files
    assert watchfolder.re_dirs == watchfolder_.re_dirs
    assert watchfolder.added_workflow == watchfolder_.added_workflow
    assert watchfolder.added_variables == watchfolder_.added_variables
    assert watchfolder.modified_workflow == watchfolder_.added_workflow
    assert watchfolder.modified_variables == watchfolder_.added_variables
    assert watchfolder.deleted_workflow == watchfolder_.added_workflow
    assert watchfolder.deleted_variables == watchfolder_.added_variables

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_delete(application: MainApplication):
    """
    Test delete workflow

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    variables = {"source": "/tmp"}
    watchfolder = application.watchfolders.create(
        "/tmp/madam_tests",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
    )

    watchfolders = application.watchfolders.list(
        status=WATCHFOLDER_STATUS_STOPPED, workflow=workflow
    )
    assert len(watchfolders) == 1

    application.watchfolders.delete(watchfolders[0])

    watchfolders = application.watchfolders.list(
        status=WATCHFOLDER_STATUS_STOPPED, workflow=workflow
    )
    assert len(watchfolders) == 0

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_list(application: MainApplication):
    """
    Test list watchfolders

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()
        application.workflows.create(workflow_content)

    workflow = application.workflows.create(workflow_content)
    variables = {"source": "/tmp"}

    # empty list
    watchfolders = application.watchfolders.list(workflow=workflow)
    assert len(watchfolders) == 0

    # 10 instances
    for _ in range(0, 10):
        application.watchfolders.create(
            "/tmp/madam_tests",
            added_workflow=workflow,
            added_variables=variables,
            modified_workflow=workflow,
            modified_variables=variables,
            deleted_workflow=workflow,
            deleted_variables=variables,
        )

    watchfolders = application.watchfolders.list(workflow=workflow)
    assert len(watchfolders) == 10

    # Paginate forward with 5 elements per page
    watchfolders = application.watchfolders.list(
        offset=5, limit=5, sort_column="id", workflow=workflow
    )
    assert len(watchfolders) == 5
    assert watchfolders[0].id < watchfolders[4].id

    # Descending column
    watchfolders = application.watchfolders.list(
        sort_column="id", sort_ascending=False, workflow=workflow
    )
    assert len(watchfolders) == 10
    assert watchfolders[0].id > watchfolders[9].id

    for watchfolder in watchfolders:
        application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_start(application: MainApplication):
    """
    Test start watchfolder

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    variables = {"source": "/tmp"}
    watchfolder = application.watchfolders.create(
        "/tmp/madam_tests",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
    )

    assert watchfolder.status is WATCHFOLDER_STATUS_STOPPED
    assert watchfolder.start_at is None
    assert watchfolder.end_at is None
    assert watchfolder.re_files is None
    assert watchfolder.re_dirs is None
    assert watchfolder.added_workflow == workflow
    assert watchfolder.added_variables == variables
    assert watchfolder.modified_workflow == workflow
    assert watchfolder.modified_variables == variables
    assert watchfolder.added_workflow == workflow
    assert watchfolder.added_variables == variables
    assert watchfolder.added_items_key == "added_items"
    assert watchfolder.modified_items_key == "modified_items"
    assert watchfolder.deleted_items_key == "deleted_items"

    application.watchfolders.start(watchfolder)

    assert watchfolder.status is WATCHFOLDER_STATUS_RUNNING
    assert watchfolder.start_at is not None

    sleep(1)

    with open(ADDED_FILE_PATH, "w", encoding="utf-8") as filehandle:
        filehandle.write("added file...")

    sleep(1)
    workflow_instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(workflow_instances) == 1
    workflow_instance = workflow_instances[0]
    assert workflow_instance.input == {
        "source": "/tmp",
        "added_items": [ADDED_FILE_PATH],
    }

    with open(ADDED_FILE_PATH, "a", encoding="utf-8") as filehandle:
        filehandle.write("modify file...")

    sleep(1)
    workflow_instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(workflow_instances) == 2
    workflow_instance = workflow_instances[1]
    assert workflow_instance.input == {
        "source": "/tmp",
        "modified_items": [ADDED_FILE_PATH],
    }

    os.unlink(ADDED_FILE_PATH)

    sleep(1)
    workflow_instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(workflow_instances) == 3
    workflow_instance = workflow_instances[2]
    assert workflow_instance.input == {
        "source": "/tmp",
        "deleted_items": [ADDED_FILE_PATH],
    }

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)


def test_stop(application: MainApplication):
    """
    Test stop watchfolder

    :param application: Madam Application instance
    :return:
    """
    with open(WORKFLOW_PATH, "r", encoding="utf-8") as fh:
        workflow_content = fh.read()

    workflow = application.workflows.create(workflow_content)
    variables = {"source": "/tmp"}
    watchfolder = application.watchfolders.create(
        "/tmp/madam_tests",
        added_workflow=workflow,
        added_variables=variables,
        modified_workflow=workflow,
        modified_variables=variables,
        deleted_workflow=workflow,
        deleted_variables=variables,
        added_items_key="added_items",
        modified_items_key="modified_items",
        deleted_items_key="deleted_items",
    )

    assert watchfolder.status is WATCHFOLDER_STATUS_STOPPED
    assert watchfolder.start_at is None
    assert watchfolder.end_at is None
    assert watchfolder.re_files is None
    assert watchfolder.re_dirs is None
    assert watchfolder.added_workflow == workflow
    assert watchfolder.added_variables == variables
    assert watchfolder.modified_workflow == workflow
    assert watchfolder.modified_variables == variables
    assert watchfolder.added_workflow == workflow
    assert watchfolder.added_variables == variables
    assert watchfolder.added_items_key == "added_items"
    assert watchfolder.modified_items_key == "modified_items"
    assert watchfolder.deleted_items_key == "deleted_items"

    application.watchfolders.start(watchfolder)

    assert watchfolder.status is WATCHFOLDER_STATUS_RUNNING
    assert watchfolder.start_at is not None

    application.watchfolders.stop(watchfolder.id)

    assert watchfolder.status is WATCHFOLDER_STATUS_STOPPED
    assert watchfolder.end_at is not None

    with open(ADDED_FILE_PATH, "w", encoding="utf-8") as filehandle:
        filehandle.write("added file...")

    os.unlink(ADDED_FILE_PATH)

    workflow_instances = application.workflow_instances.list(status=STATUS_COMPLETE)
    assert len(workflow_instances) == 0

    application.watchfolders.delete(watchfolder)
    application.workflows.delete(workflow)
