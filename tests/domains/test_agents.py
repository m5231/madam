# Copyright 2021 Vincent Texier
#
# This file is part of MADAM.
#
# MADAM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MADAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MADAM.  If not, see <https://www.gnu.org/licenses/>.

"""
Tests on agents domain module
"""
from pathlib import Path

import pytest
import yaml

from madam.domains.agents import (
    AgentApplicationNotFoundInConfig,
    AgentApplicationVersionNotFoundInConfig,
)
from madam.domains.application import MainApplication
from madam.domains.entities.agent import Agent
from madam.domains.entities.application import AgentApplication
from tests.conftest import TESTS_CONFIG_PATH


def test_list(application):
    """
    Test agents.list

    :param application: Madam Application instance
    :return:
    """
    agents_list = application.agents.list(only_names=True)

    assert len(agents_list) == 9
    assert agents_list[0] == "bash"
    assert agents_list[8] == "scenes"

    agents_list = application.agents.list()
    assert len(agents_list) == 9
    assert isinstance(agents_list[0], Agent)
    assert agents_list[0].name == "bash"
    assert isinstance(agents_list[8], Agent)
    assert agents_list[8].name == "scenes"


def test_read(application):
    """
    Test agents.help

    :param application: Madam Application instance
    :return:
    """
    agents_list = application.agents.list(only_names=True)
    agent = application.agents.read(agents_list[0])

    assert agent is not None
    assert isinstance(agent, Agent)
    assert agent.name == agents_list[0]
    assert agent.help != ""
    assert isinstance(agent.help, str)
    assert agent.applications is not None
    assert "bash" in agent.applications
    assert agent.applications["bash"] == AgentApplication(name="bash", version="latest")


def test_check_config_missing_application(tmp_folder):
    """
    Test error in agents.check_config

    :param tmp_folder: Tmp folder path fixture
    :return:
    """
    # load madam config
    with open(TESTS_CONFIG_PATH, encoding="utf-8") as filehandler:
        config = yaml.load(filehandler, Loader=yaml.FullLoader)

    del config["docker"]["applications"]["ffmpeg"]

    bad_config_path = Path(tmp_folder).joinpath("madam_bad_config.yaml")

    with open(bad_config_path, "w", encoding="utf-8") as filehandler:
        yaml.safe_dump(config, filehandler)

    # create new Madam application domain with exception
    with pytest.raises(Exception) as e_info:
        MainApplication(str(bad_config_path))

    assert e_info.errisinstance(AgentApplicationNotFoundInConfig)


def test_check_config_missing_application_version(tmp_folder):
    """
    Test error in agents.check_config

    :param tmp_folder: Tmp folder path fixture
    :return:
    """
    # load madam config
    with open(TESTS_CONFIG_PATH, encoding="utf-8") as filehandler:
        config = yaml.load(filehandler, Loader=yaml.FullLoader)

    del config["docker"]["applications"]["ffmpeg"]["latest"]

    bad_config_path = Path(tmp_folder).joinpath("madam_bad_config.yaml")

    with open(bad_config_path, "w", encoding="utf-8") as filehandler:
        yaml.safe_dump(config, filehandler)

    # create new Madam application domain with exception
    with pytest.raises(Exception) as e_info:
        MainApplication(str(bad_config_path))

    assert e_info.errisinstance(AgentApplicationVersionNotFoundInConfig)
